<?php
require_once('include/setup.app.php');

funcFS::deleteFilesXSecondsOld($GLOBALS['app.folder.cache.sql'], $GLOBALS['app.db.cachetime']);

$content .= Date('Y-m-d H:i:s');

$GLOBALS['app.devMode'] = false;
$GLOBALS['app.profile'] = false;
$GLOBALS['app.profile.SQL'] = false;
$GLOBALS['app.profile.outputSQL'] = false;
$GLOBALS['app.profile.ajax'] = false;
funcAlert::clear();
$GLOBALS['app.ui.scripts'] = null;
funcUI::renderOutput($GLOBALS['app.ui.theme'], 'data.php');
?>