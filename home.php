<?php
require_once('include/setup.app.php');

funcUI::queueScript('js', 'bottom', 'ext', 'js/jquery/jquery.js');
funcUI::queueScript('js', 'bottom', 'ext', 'js/jquery/jquery-ui.js');

if (empty($module)) {
  $module = 'index';
}
if (empty($action)) {
  $action = 'index';
}

$initUseCache = $GLOBALS['app.db.usecache'];

if (count($_POST) > 0 || $method == 'script') {
  $GLOBALS['app.db.usecache'] = false;
  $content .= funcUI::runScript($action . '.php', $module);
  $GLOBALS['app.db.usecache'] = $initUseCache;
}
if ($module == 'cron') {
  $GLOBALS['app.db.usecache'] = false;
}
$page = funcUI::getPage($action . '.php', $module);
if ($module == 'cron') {
  $GLOBALS['app.db.usecache'] = $initUseCache;
}
if ($page === false) {
  funcCore::redirect('home.php', 'Page does not exist');
}
$content .= $page;
unset($page);

funcUI::renderOutput($GLOBALS['app.ui.theme'], 'html.php');
?>