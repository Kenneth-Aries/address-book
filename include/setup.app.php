<?php
if ($_SERVER['SERVER_ADDR']=='192.168.0.107') {
  error_reporting(E_ALL);
  ini_set('display_errors', 1);
}
else {
  error_reporting(E_ALL ^ E_WARNING ^ E_NOTICE ^ E_DEPRECATED ^ E_STRICT);
  ini_set('display_errors', 0);
}
$GLOBALS['app.timer.start'] = microtime(true);
if (isset($_SERVER['HTTP_ACCEPT_ENCODING']) && substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) ob_start("ob_gzhandler"); else ob_start();
session_start();
require_once 'mod.constants.php'; //same folder as setup.app.php
require_once $GLOBALS['app.folder.include'] . 'class.func.alert.php';
require_once $GLOBALS['app.folder.include'] . 'class.func.array.php';
require_once $GLOBALS['app.folder.include'] . 'class.func.core.php';
require_once $GLOBALS['app.folder.include'] . 'class.func.custom.php';
require_once $GLOBALS['app.folder.include'] . 'class.func.data.php';
require_once $GLOBALS['app.folder.include'] . 'class.func.date.php';
require_once $GLOBALS['app.folder.include'] . 'class.func.env.php';
require_once $GLOBALS['app.folder.include'] . 'class.func.form.php';
require_once $GLOBALS['app.folder.include'] . 'class.func.fs.php';
require_once $GLOBALS['app.folder.include'] . 'class.func.net.php';
require_once $GLOBALS['app.folder.include'] . 'class.func.string.php';
require_once $GLOBALS['app.folder.include'] . 'class.func.ui.php';
require_once $GLOBALS['app.folder.include'] . 'class.func.user.php';
require_once $GLOBALS['app.folder.include'] . 'class.func.xml.php';

require_once $GLOBALS['app.folder.include'] . 'class.db.mysql.php';

if ($GLOBALS['app.db.host'] != '' && $GLOBALS['app.db.un']!='' && $GLOBALS['app.db.pw']!='' && $GLOBALS['app.db.db']!='') {
  $GLOBALS['app.db'] = new db($GLOBALS['app.db.host'], $GLOBALS['app.db.un'], $GLOBALS['app.db.pw'], $GLOBALS['app.db.db']);
}

$content = null;
$module = funcArray::get($_REQUEST, 'module');
$action = funcArray::get($_REQUEST, 'action');
$method = funcArray::get($_REQUEST, 'method');
$menu = funcArray::get($_REQUEST, 'menu');

if (funcCore::requireClasses($GLOBALS['app.var.user.classname'])) {
  $GLOBALS['app.user'] = new $GLOBALS['app.var.user.classname'];
  $GLOBALS['app.user.loggedin'] = false;
  $loginAttempt = false;
  $username = null;
  $password = null;
  if (isset($_POST[$GLOBALS['app.var.user.username']]) && isset($_POST[$GLOBALS['app.var.user.password']]) && isset($_POST['request']) && $_POST['request'] == 'login') {
    $loginAttempt = true;
    $username = $GLOBALS['app.db']->realEscapeString($_POST[$GLOBALS['app.var.user.username']]);
    $password = $GLOBALS['app.db']->realEscapeString($_POST[$GLOBALS['app.var.user.password']]);
    $salt = '';
    if (!empty($password)) {
      if (!empty($GLOBALS['app.var.user.password.encryption']) && !empty($GLOBALS['app.var.user.password.salt'])) {
        $salt = $GLOBALS['app.var.user.password.salt'];
      }

      if ($GLOBALS['app.var.user.password.encryption'] == 'sha1') {
        $password = sha1($password . $salt);
      }
      elseif ($GLOBALS['app.var.user.password.encryption'] == 'md5') {
        $password = md5($password . $salt);
      }
    }

    if (empty($username) || empty($password)) {
      //attempted login but at least one field blank
      session_destroy();
      session_start();
      funcCore::redirect(null, 'Please enter your username and password');
    }
  }
  elseif (isset($_SESSION[$GLOBALS['app.var.user.username']]) && isset($_SESSION[$GLOBALS['app.var.user.password']])) {
    $username = $_SESSION[$GLOBALS['app.var.user.username']];
    $password = $_SESSION[$GLOBALS['app.var.user.password']];
  }

  if (!empty($username) && !empty($password)) {
    //check username and password and if active
    $uArray = array($GLOBALS['app.var.user.username'] => $username, $GLOBALS['app.var.user.password'] => $password);
    if (!empty($GLOBALS['app.var.user.activestatus.field'])) {
      $uArray = array_merge($uArray, array($GLOBALS['app.var.user.activestatus.field'] => $GLOBALS['app.var.user.activestatus.active']));
    }
    if ($GLOBALS['app.user']->lookup($uArray)) {
      $_SESSION[$GLOBALS['app.var.user.username']] = $username;
      $_SESSION[$GLOBALS['app.var.user.password']] = $password;
      $GLOBALS['app.user.loggedin'] = true;
      if ($loginAttempt && isset($GLOBALS['app.var.user.redirectafterlogin']) && !empty($GLOBALS['app.var.user.redirectafterlogin'])) {
        funcCore::redirect($GLOBALS['app.var.user.redirectafterlogin']);
      }
    }
    else {
      session_destroy();
      session_start();
      funcCore::redirect(null, 'Username and/or password incorrect');
    }
  }
}

$GLOBALS['app.timer.loaded'] = microtime(true);
?>