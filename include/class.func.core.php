<?php
class funcCore {

  static function getAvailableClasses() {
    $classes = get_declared_classes();
    sort($classes);
    return $classes;
  }
  
  /**
   * You can pass in a single class name: e.g. 'user' 
   * or you can pass in multiple class names in a comma-separated list: e.g. 'user,log,links'
  **/
  static function requireClasses($classes) {
    $result = true;
    if (empty($classes)) {
      return false;
    }
    $classes_arr = explode(',', strtolower($classes));
    if (is_array($classes_arr) && count($classes_arr)>0) {
      foreach ($classes_arr as $class) {
        $class = trim($class);
        $file = $GLOBALS['app.folder.include.generic'].'class.' . $class . '.php';
        if (is_file($file)) {
          require_once($file);
        }
        else {
          $result = false;
        }
      }
    }
    return $result;
  }
  
  static function iif($var, $value, $true, $false) {
    if ($var == $value) {
      return $true;
    }
    else {
      return $false;
    }
  }

  /**
   * $level = warning, error or success
   * $level defaults to warning
  **/
  static function redirect($url = null, $alert=null, $level=null) {
    if (!is_null($alert)) {
      funcAlert::add($alert, $level, true);
    }
    $alerts = funcAlert::get($GLOBALS['app.alert.all'], false);
    if ($alerts) {
      foreach ($alerts as $a) {
        funcAlert::add($a['text'], $a['level'], true);
      }
    }
    $url = ((is_null($url) || $url == '') ? funcEnv::currentURLPath() : $url);
    header('location: ' . $url);
    exit();
  }

  static function pushFile($fileContent, $fileName, $metaContentType) {
    header('Content-Type: ' . $metaContentType);
    header('Content-Disposition: attachment; filename=' . $fileName);
    header('Pragma: no-cache');
    header('Expires: 0');
    echo $fileContent;
    exit();
  }

  static function exec($cmd, $input='') {
    $proc = proc_open($cmd, array(0=>array('pipe', 'r'), 1=>array('pipe', 'w'), 2=>array('pipe', 'w')), $pipes);
    fwrite($pipes[0], $input);
    fclose($pipes[0]);
    $stdout = stream_get_contents($pipes[1]);
    fclose($pipes[1]);
    $stderr = stream_get_contents($pipes[2]);
    fclose($pipes[2]);
    $rtn = proc_close($proc);
    return array('stdout' => $stdout,
                 'stderr' => $stderr,
                 'return' => $rtn); 
  }

  static function execCronUrl($url, $waitForResponse = false) {
    if ($waitForResponse === false) {
      $cmd = 'wget -q -O /dev/null "' . $url . '" > /dev/null 2>&1 &';
      exec($cmd);
      return true;
    }
    else {
      $cmd = 'wget -qO- "' . $url . '"';
      return funcCore::exec($cmd);
    }
  }

}
?>