<?php
require_once($GLOBALS['app.folder.include'] . 'class.db.mysql.php');
$filename = $GLOBALS['app.folder.include.extend'] . 'class.contacts.extend.php';

if (is_file($filename)) {
  require_once($filename);
}
else {
  class Contacts extends _Contacts {
  }
}

class _Contacts {
  public $Id;
  public $FirstName;
  public $Surname;
  public $ContactNumbers;
  public $EmailAddresses;

  public function __construct($Id = null) {
    if (!is_null($Id)) {
      $this->lookup($Id);
    }
  }

  private function getPrimaryKey($classOrDB = 'class') {
    if (strtolower($classOrDB) == 'class') {
      return 'Id';
    }
    elseif (strtolower($classOrDB) == 'db') {
      return 'Id';
    }
    return null;
  }

  public function exists($Id = null) {
    $Id = (!empty($Id)) ? $GLOBALS['app.db']->realEscapeString($Id) : $this->Id;
    if (empty($Id)) {
      return false;
    }

    $sql = "SELECT `Id` FROM tblcontacts WHERE 
`Id` = '" . $GLOBALS['app.db']->realEscapeString($Id) . "'";

    $rows = $GLOBALS['app.db']->executeQuery($sql, true);
    $result = (isset($rows) && $rows) ? true : false;
    return $result;
  }

  public function lookup($value = null) {
    if (!is_array($value)) {
      $value = (!is_null($value)) ? $GLOBALS['app.db']->realEscapeString($value) : $this->Id;
      if (is_null($value) || $value == '') {
        return false;
      }
      $field = 'Id';
      $value = array($field => $value);
    }
    elseif (count($value) == 0) {
      return false;
    }

    $where = array();
    foreach ($value as $field => $v) {
      $field = $GLOBALS['app.db']->realEscapeString($field);
      if (is_null($v)) {
        $v = 'IS NULL';
      }
      else {
        $v = '= \'' . $GLOBALS['app.db']->realEscapeString($v) . '\'';
      }
      $where[] = "`$field` $v";
    }
    $where = implode(' AND ', $where);

    $sql = "SELECT * FROM tblcontacts WHERE $where LIMIT 1";
    $rows = $GLOBALS['app.db']->executeQuery($sql, true);
    if (isset($rows) && $rows) {
      $this->populate($rows[0]);
      return true;
    }
    return false;
  }

  static function get($select = null, $where = null, $order = null, $join = null, $page = null, $rpp = null, $countOnly = false, $sqlOnly = false, $arrayInsteadOfClass = false) {
    $select = (empty($select)) ? '`tblcontacts`.*' : $select;
    $where = (empty($where)) ? '' : "WHERE $where";
    $order = (empty($order)) ? '' : 'ORDER BY ' . $order;
    $join = (empty($join)) ? '' : $join;
    $page = (empty($page)) ? 1 : abs((int)$page);
    $limit = (is_numeric($rpp)) ? 'LIMIT ' . ($page - 1) * $rpp . ',' . $rpp : '';
    if ($countOnly) {
      $sql = "SELECT COUNT(DISTINCT `tblcontacts`.`Id`) AS `Total` FROM tblcontacts $join $where";
    }
    else {
      $sql = "SELECT $select FROM tblcontacts $join $where $order $limit";
    }
    if ($sqlOnly) {
      return $sql;
    }
    $rows = $GLOBALS['app.db']->executeQuery($sql, true);
    if (isset($rows) && $rows) {
      if ($arrayInsteadOfClass) {
        return $rows;
      }
      if ($countOnly) {
        return $rows[0]['Total'];
      }
      $result = Array();
      foreach ($rows as $row) {
        $c = new Contacts();
        $c->populate($row);
        $result[$c->Id] = $c;
      }
      return $result;
    }
    return null;
  }

  static function count($where = null, $join = null) {
    return self::get(null, $where, null, $join, null, null, true);
  }

  static function sql($select = null, $where = null, $order = null, $join = null, $page = null, $rpp = null, $countOnly = null) {
    return self::get($select, $where, $order, $join, $page, $rpp, $countOnly, true);
  }

  function populate($row) {
    $this->Id = isset($row['Id']) ? funcString::latinToUtf8($row['Id']) : null;
    $this->FirstName = isset($row['FirstName']) ? funcString::latinToUtf8($row['FirstName']) : null;
    $this->Surname = isset($row['Surname']) ? funcString::latinToUtf8($row['Surname']) : null;
    $this->ContactNumbers = isset($row['ContactNumbers']) ? funcString::latinToUtf8($row['ContactNumbers']) : null;
    $this->EmailAddresses = isset($row['EmailAddresses']) ? funcString::latinToUtf8($row['EmailAddresses']) : null;
  }

  public function save() {
    if ($this->exists()) {
      $sql = "UPDATE tblcontacts SET
`FirstName` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->FirstName)), true, true) . ",
`Surname` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Surname)), true, true) . ",
`ContactNumbers` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->ContactNumbers)), true, true) . ",
`EmailAddresses` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->EmailAddresses)), true, true) . "
WHERE
`Id` = '" . $GLOBALS['app.db']->realEscapeString($this->Id) . "'";
    }
    else {
      $sql = "INSERT INTO tblcontacts SET
`FirstName` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->FirstName)), true, true) . ",
`Surname` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->Surname)), true, true) . ",
`ContactNumbers` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->ContactNumbers)), true, true) . ",
`EmailAddresses` = " . funcData::nz($GLOBALS['app.db']->realEscapeString(funcString::utf8ToLatin($this->EmailAddresses)), true, true) . "";
    }
    $result = $GLOBALS['app.db']->executeSQL($sql);
    $result = ($result == 1) ? true : false;
    $this->Id = (empty($this->Id) && $result) ? $GLOBALS['app.db']->lastInsertId() : $this->Id;
    return $result;
  }

  public function delete($Id = null) {
    $Id = (!empty($Id)) ? $GLOBALS['app.db']->realEscapeString($Id) : $this->Id;
    if (empty($Id)) {
      return false;
    }
    $sql = "DELETE FROM tblcontacts WHERE `Id` = '$Id' LIMIT 1";
    $result = $GLOBALS['app.db']->executeSQL($sql);
    return ($result == 1) ? true : false;
  }

}
?>