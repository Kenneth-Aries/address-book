<?php
class funcEnv {
  static function currentIP() {
    return $_SERVER['REMOTE_ADDR'];
  }

  /**
   * Returns current URL path - i.e. without filename
   */
  static function currentURLPath() {
    if (isset($GLOBALS['app.cache.currentURLPath'])) {
      return $GLOBALS['app.cache.currentURLPath'];
    }
    $server = $_SERVER['SERVER_NAME'];
    $path = explode('/', $_SERVER['REQUEST_URI']);
    array_pop($path);
    $GLOBALS['app.cache.currentURLPath'] = 'http'.(isset($_SERVER['HTTPS']) ? 's' : '').'://'.$server.implode('/', $path).'/';
    return $GLOBALS['app.cache.currentURLPath'];
  }

  /**
   * Returns current URL - i.e. with filename (with or without the querystring)
   */
  static function currentURL($include_querystring = false, $htmlSafe = true) {
    if (isset($GLOBALS['app.cache.currentURL.include_querystring.'.($include_querystring ? 'true' : 'false')])) {
      return $GLOBALS['app.cache.currentURL.include_querystring.'.($include_querystring ? 'true' : 'false')];
    }
    $server = $_SERVER['SERVER_NAME'];
    $path = $_SERVER['REQUEST_URI'];
    if (!$include_querystring) {
      $path = explode('?', $path);
      $path = $path[0];
    }
    if ($htmlSafe) {
      $path = str_replace('&', '&amp;', $path);
    }
    $GLOBALS['app.cache.currentURL.include_querystring.'.($include_querystring ? 'true' : 'false')] = 'http'.(isset($_SERVER['HTTPS']) ? 's' : '').'://'.$server.$path;
    return $GLOBALS['app.cache.currentURL.include_querystring.'.($include_querystring ? 'true' : 'false')];
  }

  static function currentFilename() {
    $path = explode('/', $_SERVER['PHP_SELF']);
    return array_pop($path);
  }

}
?>