<?php
class funcString {
  
  static function br2nl($html) {
    return preg_replace('#<br\s*/?>#i', "\n", $html);
  }
  
  static function getFirstLineWithMatch($haystack, $needle) {
    $lines = explode("\n", $haystack);
    if ($lines) {
      foreach ($lines as $line) {
        if (strpos($line, $needle) !== false) {
          return $line;
        }
      }
    }
    return null;
  }

  static function shorten($str, $len) {
    if (strlen($str) > $len) {
      $str = substr($str, 0, $len) . '...';
    }
    return $str;
  }

  static function toProper($str) {
    return ucwords(strtolower($str));
  }

  static function toSentence($str) {
    return ucfirst(strtolower($str));
  }

  static function filter($str, $filter_type) {
    if (empty($str) || empty($filter_type)) {
      return $str;
    }
    if ($filter_type == 'ALPHANUMERIC') {
      $allowedChars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    }
    elseif ($filter_type == 'DIGIT') {
      $allowedChars = '0123456789';
    }
    elseif ($filter_type == 'FILENAME') {
      $allowedChars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789._- ()';
    }
    elseif ($filter_type == 'WORDS') {
      $allowedChars = 'abcdefghijklmnopqrstuvwxyz- \'';
    }
    else {
      return $str;
    }

    $output = null;
    $i = 0;
    $max = strlen($str);
    for($i=0; $i<$max; $i++) {
      $letter = substr($str, $i, 1);
      if (strpos($allowedChars, $letter) !== false) {
        $output .= $letter;
      }
    }
    return $output;
  }

  static function firstWord($text){
    return substr($text, 0, strpos($text, ' '));
  }

  static function smsParts($text) {
    if (strlen($text) <= 160) {
      return 1;
    }
    else {
      return ceil(strlen($text) / 152);
    }
  }

  private function valid_1byte($char) {
    if (!is_int($char)) return false;
    return ($char & 0x80) == 0x00;
  }

  private function valid_2byte($char) {
    if (!is_int($char)) return false;
    return ($char & 0xE0) == 0xC0;
  }

  private function valid_3byte($char) {
    if (!is_int($char)) return false;
    return ($char & 0xF0) == 0xE0;
  }

  private function valid_4byte($char) {
    if (!is_int($char)) return false;
    return ($char & 0xF8) == 0xF0;
  }

  private function valid_nextbyte($char) {
    if (!is_int($char)) return false;
    return ($char & 0xC0) == 0x80;
  }

  public static function valid_utf8($string) {
    $len = strlen($string);
    $i = 0;
    while ($i < $len) {
      $char = ord(substr($string, $i++, 1));
      if (self::valid_1byte($char)) {    // continue
        continue;
      }
      else if (self::valid_2byte($char)) { // check 1 byte
        if (!self::valid_nextbyte(ord(substr($string, $i++, 1)))) return false;
      }
      else if (self::valid_3byte($char)) { // check 2 bytes
        if (!self::valid_nextbyte(ord(substr($string, $i++, 1)))) return false;
        if (!self::valid_nextbyte(ord(substr($string, $i++, 1)))) return false;
      }
      else if (self::valid_4byte($char)) { // check 3 bytes
        if (!self::valid_nextbyte(ord(substr($string, $i++, 1)))) return false;
        if (!self::valid_nextbyte(ord(substr($string, $i++, 1)))) return false;
        if (!self::valid_nextbyte(ord(substr($string, $i++, 1)))) return false;
      } // goto next char
    }
    return true; // done
  }

  public static function utf8ToLatin($value) {
    if (!empty($value)) {
      return iconv("UTF-8", "Windows-1252//IGNORE", $value);
    }
  }

  public static function latinToUtf8($value) {
    if (!empty($value)) {
      return iconv("Windows-1252", "UTF-8//IGNORE", $value);
    }
  }

}
?>