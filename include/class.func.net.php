<?php
class funcNet {
  
  static function shortenURL($url) {
    $bitlyURL = 'http://api.bit.ly/shorten?version=2.0.1&' .
                  'longUrl=' . urlencode($url) .
                  '&login=' . $GLOBALS['app.var.bitly.user'] .
                  '&apiKey=' . $GLOBALS['app.var.bitly.apikey'] .
                  '&format=xml';
    $result = funcNet::httpCurl($bitlyURL);
    $xml = simplexml_load_string($result['body']);
    if ($xml === false || empty($xml->results->nodeKeyVal->hash)) {
      //problem shortening so return the URL instead of nothing
      return $url;
    }
    return 'http://bit.ly/' . $xml->results->nodeKeyVal->hash;
  }

  static function httpCurl($strURL, $strParams = null, $strMethod = 'GET', $postHeader = null) {
    $ch = curl_init();
    if (is_array($postHeader)) {
      curl_setopt($ch, CURLOPT_HTTPHEADER, $postHeader);
    }
    if ($strMethod=='POST') {
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $strParams);
    }
    else {
      if (!is_null($strParams)) {
        $strURL .= '?' . $strParams;
      }
      curl_setopt($ch, CURLOPT_HTTPGET, 1);
    }
    curl_setopt($ch, CURLOPT_URL, $strURL);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_TIMEOUT, 300);
  
    $return = curl_exec($ch);
    
    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    
    $result = array( 'header' => '', 
                     'body' => '', 
                     'curl_error' => '', 
                     'http_code' => '',
                     'last_url' => '');
    $result['header'] = substr($return, 0, $header_size);
    $result['body'] = substr($return, $header_size);
    $result['http_code'] = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    $result['last_url'] = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
    $result['curl_error'] = curl_error($ch);
    
    curl_close($ch);
    
    return $result;
  }

  static function downloadFile($url, $filename) {
    if (!is_file($filename)) {
      if (!touch($filename)) {
        return 'touch failed';
      }
    }
    if (!unlink($filename)) {
      return 'unlink failed';
    }
    $file = fopen($url, "rb");
    if ($file) {
      $newf = fopen($filename, "wb");
      if ($newf)
      while(!feof($file)) {
        fwrite($newf, fread($file, 1024 * 8 ), 1024 * 8 );
      }
      fclose($file);
    }
    else {
      return 'fopen failed';
    }
    
    if ($newf) {
      fclose($newf);
      return true;
    }
    else {
      return 'fwrite failed';
    }
  }

}
?>