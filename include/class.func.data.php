<?php
class funcData {

  static function nz($value, $acceptsNull = false, $str = false, $default = null) {
    if ((is_null($value) || $value == '') && (is_null($default) || $default == '')) {
      if ($acceptsNull) {
        return 'NULL';
      }
      else {
        return ($str ? "''" : '0');
      }
    }
    $result = (is_null($value) ? $default : $value);
    return ($str ? "'$result'" : $result);
  }
  
  /**
    Validate an email address.
    Provide email address (raw input)
    Returns true if the email address has the email 
    address format and the domain exists.
  */
  static function validateEmail($email, $doDNScheck = false) {
    $isValid = true;
    $atIndex = strrpos($email, "@");
    if (is_bool($atIndex) && !$atIndex) {
      $isValid = false;
    }
    else {
      $domain = substr($email, $atIndex+1);
      $local = substr($email, 0, $atIndex);
      $localLen = strlen($local);
      $domainLen = strlen($domain);
      if ($localLen < 1 || $localLen > 64) {
        // local part length exceeded
        $isValid = false;
      }
      else if ($domainLen < 1 || $domainLen > 255) {
        // domain part length exceeded
        $isValid = false;
      }
      else if ($local[0] == '.' || $local[$localLen-1] == '.'){
        // local part starts or ends with '.'
        $isValid = false;
      }
      else if (preg_match('/\\.\\./', $local)){
        // local part has two consecutive dots
        $isValid = false;
      }
      else if (!preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain)){
        // character not valid in domain part
        $isValid = false;
      }
      else if (preg_match('/\\.\\./', $domain)){
        // domain part has two consecutive dots
        $isValid = false;
      }
      else if (!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/', str_replace("\\\\","",$local))) {
        // character not valid in local part unless 
        // local part is quoted
        if (!preg_match('/^"(\\\\"|[^"])+"$/', str_replace("\\\\","",$local))) {
          $isValid = false;
        }
      }
      if ($doDNScheck && $isValid && !(checkdnsrr($domain, "MX") || checkdnsrr($domain, "A"))) {
        // domain not found in DNS
        $isValid = false;
      }
    }
    return $isValid;
  }

  static function exportSqlToCsv($sql, $headers = true) {
    $export = $GLOBALS['app.db']->executeSQL($sql);
    $fields = mysql_num_fields($export);
    $header = '';
    $data = '';
    
    if ($headers) {
      for ($i = 0; $i < $fields; $i++) {
        $header .= mysql_field_name($export, $i) . ",";
      }
      $header = $header . "\n";
    }
    
    while($row = mysql_fetch_row($export)) {
      $line = '';
      foreach($row as $value) {
        if (!isset($value) || $value == '') {
          $value = ",";
        }
        else {
          $value = str_replace('"', '""', $value);
          $value = '"'.$value.'"' . ",";
        }
        $line .= $value;
      }
      $data .= trim($line) . "\n";
    }
    $data = str_replace("\r", '', $data);
    return $header . $data;
  }

  static function csvField($field) {
    //check for comma or quotes
    if (strpos($field, ',')!==false || strpos($field, '"')!==false) {
      //found comma or quotes in field - double-up the quotes within and put quotes around
      $field = '"'.str_replace('"', '""', $field).'"';
    }
    return $field;
  }
  static function html2xls($htmltable, &$xls) {
    //PHPExcel must be passed into $xls by reference
    $xls->setActiveSheetIndex(0);
    
//    $simpleXML = simplexml_load_string($htmltable);
//echo funcArray::display($simpleXML);
//exit;

    $dom = new DOMDocument;
    $result = $dom->loadHTML($htmltable);
    $rows = $dom->getElementsByTagName('tr');
    foreach ($rows as $rowNum => $row) {
      $columns = $row->getElementsByTagName('th');
      if ($columns->length == 0) {
        $columns = $row->getElementsByTagName('td');
      }
      if ($columns->length > 0) {
        /** Construct column number array **/
        $columnNum = array();
        $columnNum[$xls->getActiveSheet()->getTitle()] = 0;
        foreach ($columns as $fieldNum => $field) {
          $xls->getActiveSheet()->setCellValue(funcData::num2alpha($fieldNum) . ($rowNum + 1), $field->nodeValue);
          $columnNum[$xls->getActiveSheet()->getTitle()] += 1;
        }
      }
    }
  }
  static function num2alpha($n){
      for($r = ""; $n >= 0; $n = intval($n / 26) - 1)
          $r = chr($n%26 + 0x41) . $r;
      return $r;
  }
  static function searchArray($needle, $haystack, $key){
        foreach ($haystack as $hItem => $hValue){
            
            foreach($hValue as $subHItem => $details){
            
                if($details[$key] === $needle){
                    return $hItem;
                }
            }
        }
    }
  
  static function str_getcsv2($input, $delimiter = ',', $enclosure = '"') {
    if( ! preg_match("/[$enclosure]/", $input) ) {
      return (array)preg_replace(array("/^\\s*/", "/\\s*$/"), '', explode($delimiter, $input));
    }

    $token = "##"; $token2 = "::";
    $t1 = preg_replace(array("/\\\[$enclosure]/", "/$enclosure{2}/",
         "/[$enclosure]\\s*[$delimiter]\\s*[$enclosure]\\s*/", "/\\s*[$enclosure]\\s*/"),
         array($token2, $token2, $token, $token), trim(trim(trim($input), $enclosure)));
  
    $a = explode($token, $t1);


    foreach($a as $k=>&$v) {
      if ( preg_match("/^{$delimiter}/", $v) || preg_match("/{$delimiter}$/", $v) ) {
          //$a[$k] = trim($v, $delimiter);
          if (substr($a[$k], 0, 1) == $delimiter) {
            $a[$k] = substr($a[$k], 1);
          }
          if (substr($a[$k], -1) == $delimiter) {
            $a[$k] = substr($a[$k], 0, strlen($a[$k]) - 1);
          }
          $a[$k] = preg_replace("/$delimiter/", "$token", $a[$k]); 
      }
    }
    $a = explode($token, implode($token, $a));
    return (array)preg_replace(array("/^\\s/", "/\\s$/", "/$token2/"), array('', '', $enclosure), $a);
  }

}
?>