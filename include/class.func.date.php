<?php
class funcDate {
  
  static function beginOfMonth($date) {
    return date('Y-m-01 00:00:00', $date);
  }
  
  static function beginOfNextMonth($date) {
    return date('Y-m-d', strtotime(self::beginOfMonth($date) . ' +1 month'));
  }
  
  static function endOfMonth($date, $includeTime = false) {
    return date('Y-m-d', strtotime(self::beginOfNextMonth($date) . ' -1 day'));
  }
  
  static function ZA2mySQL($date) {
    if (substr($date, 2, 1) == '/' && substr($date, 5, 1) == '/') {
      //assuming that the date is first - i.e. d/m/Y H:i:s
      return substr($date, 6, 4) . '-' . substr($date, 3, 2) . '-' . substr($date, 0, 2) . substr($date, 10);
    }
    return date('Y-m-d H:i:s', strtotime($date));
  }
  
  static function makeDatePartTwoDigit($digit) {
    if (strlen($digit) == 1) {
      return "0" . $digit;
    } 
    else {
      return $digit;
    }
  }

  static function format($date, $include_time=true) {
    if (empty($date) || $date=='0000-00-00 00:00:00') {
      return null;
    }
    if ($include_time) {
      return Date('d M Y H:i:s', strtotime($date));
    }
    else {
      return Date('d M Y', strtotime($date));
    }
  }
  
  static function now($asInteger = false) {
    return $asInteger ? strtotime('now') : date('Y-m-d H:i:s');
  }
  
  static function timePassed($date1, $date2 = null) {
    if (is_null($date2)) {
      $date2 = funcDate::now();
    }
    $blocks = array(
          array('name'=>'year',   'amount'    =>    60*60*24*365    ),
          array('name'=>'month',  'amount'    =>    60*60*24*31     ),
          array('name'=>'week',   'amount'    =>    60*60*24*7      ),
          array('name'=>'day',    'amount'    =>    60*60*24        ),
          array('name'=>'hour',   'amount'    =>    60*60           ),
          array('name'=>'minute', 'amount'    =>    60              ),
          array('name'=>'second', 'amount'    =>    1               )
        );
   
    $diff = abs(strtotime($date1)-strtotime($date2));
   
    $levels = 2;
    $current_level = 1;
    $result = array();
    foreach($blocks as $block) {
      if ($current_level > $levels) {break;}
      if ($diff/$block['amount'] >= 1) {
        $amount = floor($diff/$block['amount']);
        if ($amount!=1) {$plural='s';} else {$plural='';}
        $result[] = $amount.' '.$block['name'].$plural;
        $diff -= $amount*$block['amount'];
        $current_level++;
      }
    }
    if ($result) {
      return implode(' ',$result).' ago';
    }
    else {
      return 'now';
    }
  }

}
?>