<?php
class funcArray {
  
  static function get($array, $key, $defaultValue = null) {
    if (is_array($array)) {
      if (isset($array[$key])) {
        return $array[$key];
      }
    }
    return $defaultValue;
  }

  static function toCSV($rowArray) {
    if (is_array($rowArray) && count($rowArray)) {
      $csv = array();
      foreach ($rowArray as $field) {
        $csv[] = funcData::csvField($field);
      }
      return implode(',', $csv);
    }
    return null;
  }

  static function fromCSV($rowCSV) {
    if (!is_null($rowCSV)) {
      $rowArray = null;
      $fh = fopen('php://memory', 'rw');
      fwrite($fh, $rowCSV);
      fseek($fh, 0);
      $rowArray = fgetcsv($fh);
      fclose($fh);
      return $rowArray;
    }
    return null;
  }

  static function classesToSelectOptions($arrayOfClasses, $value, $text) {
    if (is_array($arrayOfClasses) && count($arrayOfClasses)) {
      $result = array();
      foreach ($arrayOfClasses as $a) {
        $result[] = funcForm::prepOption($a->$value, $a->$text);
      }
      return $result;
    }
    return null;
  }

  static function toSelectOptions($array) {
    if (!is_array($array) || count($array) == 0) {
      return null;
    }
    $options = array();
    foreach ($array as $k => $v) {
      $options[] = funcForm::prepOption($k, $v);
    }
    return $options;
  }

  static function arrayToFormOptions($array) {
    return funcArray::toSelectOptions($array);
  }
  
  static function getFirstKey($array) {
    if (is_array($array) && count($array)>0) {
      $keys = array_keys($array);
      return $keys[0];
    }
    return null;
  }

  static function getKeyByNum($array, $num) {
    if (is_array($array) && count($array)>0) {
      $keys = array_keys($array);
      return isset($keys[$num]) ? $keys[$num] : null;
    }
    return null;
  }

  static function getFirstItem($array) {
    $key = funcArray::getFirstKey($array);
    if (!is_null($key)) {
      return $array[$key];
    }
    return null;
  }

  static function getItemByNum($array, $num) {
    $key = funcArray::getKeyByNum($array, $num);
    if (!is_null($key) && isset($array[$key])) {
      return $array[$key];
    }
    return null;
  }

  static function arrayToSelectArray($source) {
    return funcArray::toSelectOptions($source);
  }
  
  static function rowSourceToArray($rowSource, $columns = 2) {
    if ($columns <= 0) {
      return null;
    }
    $source = explode(';', $rowSource);
    if (is_array($source) && count($source) > 0) {
      $dest = array();
      for($i=0;$i<count($source);$i=$i+$columns) {
        if ($columns == 1) {
          $dest[$source[$i]] = null;
        }
        elseif ($columns == 2) {
          $dest[$source[$i]] = $source[$i+1];
        }
        else {
          $otherFields = array_slice($source, $i+1, $i+$columns);
          $dest[$source[$i]] = $otherFields;
        }
      }
      return $dest;
    }
    return null;
  }

  static function getTopHalf($arr) {
    //if even - divide by two
    //if odd - divide by two and add one
    $count = count($arr);
    return array_slice($arr, 0, ceil($count/2));
  }

  static function getBottomHalf($arr) {
    //if even - divide by two
    //if odd - divide by two and add one
    $count = count($arr);
    $arr = array_reverse($arr, true);
    return array_reverse(array_slice($arr, 0, floor($count/2)));
  }
  
  static function display($arr, $nopre = false) {
    if ($nopre) {
      return print_r($arr, true);
    }
    else {
      return '<pre>'.print_r($arr, true).'</pre>';
    }
  }

  static function removeItemByValue($array, $val = '', $preserveKeys = true) {
  	if (empty($array) || !is_array($array)) return false;
  	if (!in_array($val, $array)) return $array;
  	
  	foreach($array as $key => $value) {
  		if ($value == $val) unset($array[$key]);
  	}
  	
  	return ($preserveKeys === true) ? $array : array_values($array);
  } //END function removeItemByValue

  static function arrayToJson($array) {
    if (!is_array($array)){
      return false;
    }
    $associative = count(array_diff(array_keys($array), array_keys(array_keys($array))));
    if ($associative){
      $construct = array();
      foreach ($array as $key => $value){
        // We first copy each key/value pair into a staging array,
        // formatting each key and value properly as we go.
        // Format the key:
        if (is_numeric($key)){
          $key = "key_$key";
        }
        $key = '"' . addslashes($key) . '"';
        // Format the value:
        if (is_array($value)) {
          $value = self::arrayToJson($value);
        }
        elseif (!is_numeric($value) || is_string($value)){
          $value = '"' . addslashes($value) . '"';
        }
        // Add to staging array:
        $construct[] = "$key: $value";
      }
      // Then we collapse the staging array into the JSON form:
      $result = '{ ' . implode(", ", $construct) . ' }';
    }
    else { // If the array is a vector (not associative):
      $construct = array();
      foreach ($array as $value){
        // Format the value:
        if (is_array($value)){
          $value = self::arrayToJson($value);
        }
        elseif (!is_numeric($value) || is_string($value)){
          $value = '\'' . addslashes($value) . '\'';
        }
        // Add to staging array:
        $construct[] = $value;
      }
      // Then we collapse the staging array into the JSON form:
      $result = '[ ' . implode(', ', $construct) . ' ]';
    }
    return $result;
  } //END function array_to_json

  static function toHTMLTable($rows) {
    $output = null;
    if (count($rows) > 0) {
      $output .= '
<table>
  <thead>
    <tr>
      <th>' . implode('</th><th>', array_keys(current($rows))) . '</th>
    </tr>
  </thead>
  <tbody>';
      foreach ($rows as $row) {
        array_map('htmlentities', $row);
        $output .= '
    <tr>
      <td>' . implode('</td><td>', $row) . '</td>
    </tr>';
      }
      $output .= '  
  </tbody>
</table>';
    }
    return $output;
  }

}
?>