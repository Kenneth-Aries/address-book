<?php

/**
 * Google chart API wrapper class
 * require_once $GLOBALS['app.folder.include'] . 'class.func.imaginggooglechart.php';
 * 
 * $chart = new funcImagingGoogleChart('Skeleton Cove Pirate Report', 'Vertical Grouped Bar Chart', 700, 400, 100, 10);
 * 
 * $chart->addLegendText('Gold coins');
 * $chart->addAxisLabel(77, 'x');
 * $chart->addDatum(77);
 * 
 * $chart->addLegendText('Unopened treasure chests');
 * $chart->addAxisLabel(6, 'x');
 * $chart->addDatum(6);
 * 
 * $chart->addLegendText('Bottles of rum');
 * $chart->addAxisLabel(34, 'x');
 * $chart->addDatum(34);
 * 
 * $chart->addLegendText('Parrots');
 * $chart->addAxisLabel(1, 'x');
 * $chart->addDatum(1);
 * 
 * $imgSrc = $chart->getUri();
 * 
 * $content .= '<img src="' . $imgSrc . '" ?>';
 *
 */

class funcImagingGoogleChart {
  const API_URI = 'http://chart.apis.google.com/chart';
  const EXTENSION = 'png';
  
  private $chart;
  private $title;
  private $axisLabels;
  private $chartType;
  private $unqKey;
  private $client;
  private $dataSet;
  private $legend;
  private $width;
  private $height;
  var $params;
  var $datum;
  private $baseForCalc = 100;
  private $axisTickFrequency = 10;
  private $colors;
  private $colorCount = 0;
  //private $arrColor = array('9E9E9E', '070057', '570024', '005733', '003E94', '4F5700', '570700', '330057', '0057D1', 'D17A00', '573300');
  private $arrColor = array('082c76', '2e56ab', '4f7cd8', '304062', '5d6d8f', '8494b6', '939393');
   
  public function __construct($title, $type, $width, $height, $baseForCalc, $axisTickFrequency) {
    $this->title = $title;
    $this->chartType = $type;
    $this->width = $width;
    $this->height = $height;
    $this->baseForCalc = $baseForCalc;
    $this->axisTickFrequency = $axisTickFrequency;
    $this->axisLabels = array('x' => array(), 'y' => array());
    $this->legend = array();
  }
  
  public function getUri() {
    if ($this->hasCachedChart()) {
      return $this->getCachedChartUri();
    }
    else {
      $this->cache();
      
      if (true || $this->hasCachedChart()) {
        return $this->getCachedChartUri();
      }
      else {
        throw new exception('Unable to cache chart');
      }
    }
  }
  
  public function addAxisLabel($label, $axis) {
    $axis = strtolower($axis);
    $label = urlencode($label);
    if ($axis == 'x') {
      array_push($this->axisLabels[$axis], $label);
    }
    elseif ($axis == 'y') {
      array_unshift($this->axisLabels[$axis], $label);
    }
  }
  
  public function addDatum($datum, $newSeries = false) {
    //must be out of 100 - thus: $datum / $this->baseForCalc * 100
    $this->dataSet .= ($newSeries && !empty($this->dataSet) ? '|' : ',') . (is_numeric($datum) ? ($datum / $this->baseForCalc * 100) : 0);
    if ($newSeries) {
      if ($this->colorCount > 0) {
        array_push($this->arrColor, array_shift($this->arrColor));
      }
      $this->colorCount = 0;
      if (!is_null($this->colors) && $this->colors != '') {
        $this->colors .= ',';
      }
    }
    if (!isset($this->arrColor[$this->colorCount])) {
      $this->colorCount = 0;
    }
    $this->colors .= $this->arrColor[$this->colorCount] . '|';
    $this->colorCount++;
  }
  
  public function addDatumSeperator() {
    $this->dataSet .= '|';
  }
  
  public function addDatumBySeries($series, $label, $datum) {
    //echo $series . '|' . $label . '|' . $datum . '<br />';
    if (!is_array($this->datum)) {
      $this->datum = array();
    }
    if (!is_array($this->datum[$series])) {
      $this->datum[$series] = array();
    }
    $this->datum[$series][$label] = (is_numeric($datum) ? ($datum / $this->baseForCalc * 100) : 0);
  }
  
  public function addLegendText($text) {
    if (in_array($text, $this->legend)) {
      return false;
    }
    array_push($this->legend, $text);
  }
  
  public function setBaseForCalc($baseForCalc) {
    $this->baseForCalc = $baseForCalc;
  }
  
  public function setWidth($width) {
    $this->width = $width;
  }
  
  public function setHeight($height) {
    $this->height = $height;
  }
  
  private function setFetchChartParams() {
    // Gradient Fill
    //GOLD = 9C8A4E
    //bright CCS blue = 11B3EA
    //dark turquoise = 55808E
    $this->params .= '&chf=c,lg,90,55808E,0,ffffff,1';
    // Axis X and Y Labels
    $this->params .= '&chxl=' . $this->getAxisLabels();
    // Axis Ranges
    $this->params .= '&chxr=1,0,' . $this->baseForCalc . ',' . $this->axisTickFrequency;
    // Axis tick mark styles
    $this->params .= '&chxt=x,y';
    // Bar width and spacing
    $this->params .= '&chbh=a,2,16';
    // Chart Size
    $this->params .= "&chs={$this->width}x{$this->height}";
    // Chart Type
    $this->params .= '&cht=' . $this->getChartType($this->chartType);
    if ($this->getChartType($this->chartType) == 'p' || $this->getChartType($this->chartType) == 'p3') {
      $this->params .= '&chp=4.712';
    }
    /*
    for ($count=0;$count<count($colors);$count++) {
      $this->params .= implode(',', $colors);
      array_push($colors, array_shift($colors));;
      if ($count<count($colors)) {
        $this->params .= '|';
      }
    }
    */
    // Chart data string
    if (!is_null($this->datum) && is_array($this->datum) && count($this->datum)) {
      $this->params .= '&chd=';
      $groupArr = funcArray::getItemByNum($this->datum, count($this->datum) - 1);
      $countSeries = count($groupArr);
      $dataSet = 't:';
      $this->colorCount = 0;
      for ($count=0;$count<$countSeries;$count++) {
        if ($this->colorCount > $countSeries) {
          array_push($this->arrColor, array_shift($this->arrColor));
          $this->colorCount = 0;
        }
        if (!is_null($this->colors) && $this->colors != '') {
          $this->colors .= ',';
        }
        if (!isset($this->arrColor[$this->colorCount])) {
          $this->colorCount = 0;
        }
        $this->colors .= $this->arrColor[$this->colorCount] . '|';
        $this->colorCount++;

        foreach ($this->datum as $group => $data) {
          $key = funcArray::getKeyByNum($groupArr, $count);
          $seriesValue = isset($data[$key]) ? $data[$key] : 0;
          $dataSet .= $seriesValue . ',';
        }
        $dataSet .= '|';
      }
      $dataSet = str_replace('t:,', 't:', $dataSet);
      $dataSet = str_replace(',|', '|', $dataSet);
      
      if ( substr($dataSet, -1, 1) == '|' ) {
        $dataSet = substr($dataSet, 0, -1);
      }
      $this->params .= $dataSet;
    }
    else {
      $this->params .= '&chd=' . $this->getDataSetString();
    }
    /*
    echo $dataSet;
    echo funcArray::display($this->datum);
    exit();
    /**/
    // Series Colours
    //$colors = array('002355', '2e56ab', '4f7cd8', '304062', '5d6d8f', '8494b6', '939393');
    $this->params .= '&chco=' . str_replace('|,', ',', substr($this->colors, 0, strlen($this->colors) - 1));
    //$this->params .= implode('|', $colors);
    // Chart legend text
    if ($this->getLegendText() != '') {
      $this->params .= '&chdl=' . $this->getLegendText();
    }
    // Grid Lines
    $this->params .= '&chg=0,20';
    // Chart Margins
    $this->params .= '&chma=|0,65';
    // Chart Title
    $this->params .= '&chtt=' . str_replace("\n", '|', $this->title);
    // Chart Style
    $this->params .= '&chts=676767,10.5';
    // Legend text location
    $this->params .= '&chdlp=b|l';
    //echo $this->params . '<br /><br />';//exit();
  }
  
  private function fetchChart() {
    $response = funcNet::httpCurl(self::API_URI, $this->params, 'POST');
    //echo funcArray::display($response);exit();
    $this->chart = $response['body'];
  }
  
  private function cache() {
    $this->fetchChart();
    file_put_contents($this->getCachedChartPath(), $this->chart);
  }
  
  private function getChartType($niceName) {
    $niceName = strtolower($niceName);
    $mapper = array(
      'bvo' => array('vertical stacked infront bar chart'),
      'bvg' => array('vertical grouped bar chart'),
      'bvs' => array('vertical stacked bar chart'),
      'bhs' => array('horizontal stacked bar chart'),
      'bhs' => array('horizontal stacked bar chart'),
      'p3' => array('3d pie chart'),
      'p' => array('pie chart')
    );
    
    foreach ($mapper as $key => $val) {
      if (in_array($niceName, $val)) {
        return $key;
      }
    }
    return $niceName;
  }
  
  private function hasCachedChart() {
    $this->setFetchChartParams();
    $this->setUnqKey();
    if (file_exists($this->getCachedChartPath())) {
      $mtime = filemtime($this->getCachedChartPath());
      return (strtotime('-1 week') > $mtime) ? false : true;
    }
    return false;
  }
  
  private function getCachedChartUri() {
    return funcEnv::currentURLPath() . 'cache/charts/' . $this->unqKey . '.' . self::EXTENSION;
  }
  
  private function getCachedChartPath() {
    return $GLOBALS['app.folder'] . 'cache/charts/' . $this->unqKey . '.' . self::EXTENSION;
  }
  
  private function setUnqKey() {
    $key = serialize($this->params);
    $key = base64_encode($key);
    $key = md5($key);
    $this->unqKey = $key;
  }

  private function getDataSetString() {
    $return = "t:{$this->dataSet}";
    $return = str_replace('t:,', 't:', $return);
    $return = str_replace('|,', '|', $return);
    
    if ( substr($return, -1, 1) == '|' ) {
      $return = substr($return, 0, -1);
    }
    
    return $return;
  }
  
  private function getAxisLabels() {
    $xAxis = null;
    $yAxis = null;
    if (is_array($this->axisLabels['x']) && count($this->axisLabels['x'])) {
      $xAxis = implode('|', $this->axisLabels['x']);
      $xAxis = str_replace(' ', '', '0:|' . $xAxis);
    }
    if (is_array($this->axisLabels['y']) && count($this->axisLabels['y'])) {
      $yAxis = implode('|', $this->axisLabels['y']);
      $yAxis = str_replace(' ', '', '1:|' . $yAxis);
    }
    return $xAxis . (!is_null($xAxis) && !is_null($yAxis) ? '|' : '') . $yAxis;
  }
  
  private function getLegendText() {
    $return = implode('|', $this->legend);
    return $return;
  }
}
