<?php
class ClassGen {

  public $table;
  public $className;
  public $fileName;
  public $primaryKey;
  public $primaryKeys; //used for multiple primary keys (save() WHERE statement)
  public $primaryKeySingle;
  public $primaryKeyMulti;
  public $output;
  public $outputExtendedClass;
  public $error;

  private $schemaDef;
  private $fields;
  private $saveFields;
  private $tableFields;
  private $populateFields;

  public function __construct($table, $className = '', $primaryKey = '') {
    if (empty($GLOBALS['app.db'])) {
      $this->error = 'App Database object not initilised';
      return false;
    }
    elseif (!$GLOBALS['app.db']->connected) {
      $this->error = 'App Database not connected';
      return false;
    }
    elseif (empty($table)) {
      $this->error = 'Table name empty';
      return false;
    }
    elseif (!in_array($table, $GLOBALS['app.db']->getTables($GLOBALS['app.db.db']))) {
      $this->error = 'Table not found in database: ' . $GLOBALS['app.db.db'];
      return false;
    }

    $this->table = $table;
    $this->primaryKeyMulti = false;

    $this->schemaDef = $GLOBALS['app.db']->executeQuery("DESCRIBE `$this->table`;");
    if (!$this->schemaDef) {
      $this->error = $GLOBALS['app.db']->errorMessage;
      return false;
    }

    $this->className = $this->classNameClean(empty($className) ? $this->table : $className);
    $this->fileName = strtolower($this->className);

    $this->fields = array();
    //do a quick run through to determine if we have a single primary key
    $countPriKey = 0;
    $this->primaryKeys = array();
    $this->primaryKeySingle = false;
    foreach ($this->schemaDef as $sd) {
      if (strpos($sd['Key'], 'PRI') !== false) {
        $countPriKey++;
        $tmp = array();
        $tmp['Var'] = $this->fieldNameClean($sd['Field']);
        $tmp['Field'] = $sd['Field'];
        $this->primaryKeys[] = $tmp;
        unset($tmp);
      }
    }
    if ($countPriKey == 1) {
      $this->primaryKeySingle = true;
    }
    elseif ($countPriKey >= 2) {
      $this->primaryKeyMulti = true;
    }

    foreach ($this->schemaDef as $sd) {
      $curField = array(
        'Var' => $this->fieldNameClean($sd['Field']),
        'Field' => $sd['Field'],
        'Type' => $sd['Type'],
        'Null' => $sd['Null'],
        'Primary' => strpos($sd['Key'], 'PRI') === false ? false : true,
        'Default' => $sd['Default'],
        'Extra' => $sd['Extra']
      );
      $this->fields[] = $curField;
      $this->tableFields .= '      $fields[\'' . $curField['Field'] . '\'] = array(\'sort\' => true, \'header\' => \'' . $curField['Field'] . '\');
';
      $this->varFields .= '  public $' . $curField['Var'] . ';
';
      $this->populateFields .= '    $this->' . $curField['Var'] . ' = isset($row[\'' . $curField['Field'] . '\']) ? funcString::latinToUtf8($row[\'' . $curField['Field'] . '\']) : null;' . "\r\n";
      if ($this->primaryKeySingle && $curField['Primary']) {
        //does not support more than one Primary Key, so override with construct parameter
        if (is_array($this->primaryKey) && count($this->primaryKey) > 0) {
          //echo $curField['Var'] . ': ' . funcArray::display($this->primaryKey) . ':' . $primaryKeyMulti . '<br />';
        }
        else {
          $this->primaryKey = array();
          $this->primaryKey['Var'] = empty($primaryKey) ? $curField['Var'] : $primaryKey;
          $this->primaryKey['Field'] = empty($primaryKey) ? $curField['Field'] : $primaryKey;
        }
      }
      else {
        //do not include primary keys - we're assuming that this is an auto incrementing primary key
        $this->saveFields .= '`' . $curField['Field'] . '` = ' . $this->sqlFormat($curField) . ',
';
      }
    }
    if (is_null($this->primaryKey)) {
      $this->primaryKey = array();
      $this->primaryKey['Var'] = empty($primaryKey) ? $this->fields[0]['Var'] : $primaryKey;
      $this->primaryKey['Field'] = empty($primaryKey) ? $this->fields[0]['Field'] : $primaryKey;
    }
    $this->saveFields = rtrim($this->saveFields, "\r\n,");

    return true;
  }

  public function writeFile($createExtendedClass = false) {
    if (empty($this->output)) {
      return false;
    }
    $file = $GLOBALS['app.folder.include.generic'] . 'class.' . $this->fileName . '.php';
    if (is_file($file)) {
      //delete first - otherwise we run into chmod issues
      if (!unlink($file)) {
        return false;
      }
    }
    $result = funcFS::writeFile($file, $this->output);
    chmod($file, 0777);

    if ($createExtendedClass) {
      $file = $GLOBALS['app.folder.include.extend'] . 'class.' . $this->fileName . '.extend.php';
      if (!is_file($file)) {
        $fw = fopen($file, 'w');
        $result = fwrite($fw, $this->outputExtendedClass);
        fclose($fw);
        chmod($file, 0777);
      }
    }

    return is_file($file);
  }

  private function isNumberType($type) {
    $type = strtoupper(substr($type, 0, strpos($type, '(')));
    switch ($type) {
      case 'TINYINT':
      case 'SMALLINT':
      case 'MEDIUMINT':
      case 'INT':
      case 'BIGINT':
      case 'FLOAT':
      case 'DOUBLE':
      case 'DECIMAL':
        return true;
    }
    return false;
  }

  private function sqlFormat($var) {
    $quotes = "'";
    $str = 'true';
    $acceptsNull = ($var['Null'] == 'YES');
    $acceptsNull = ($acceptsNull ? 'true' : 'false');
    if ($this->isNumberType($var['Type'])) {
      $quotes = "";
      $str = 'false';
    }
    if (isset($var['OverwriteDefault']) && $var['OverwriteDefault'] <> '' && $var['Default'] <> $var['OverwriteDefault']) {
      //OverwriteDefault is set, so class must set a default value instead of db engine
      $var['OverwriteDefault'] = ($var['OverwriteDefault'] == '[empty string]') ? '' : $var['OverwriteDefault'];
      $sql = '" . funcData::nz($GLOBALS[\'app.db\']->realEscapeString($this->' . $var['Var'] . '), ' . $acceptsNull . ', ' . $str . ', \'' . $var['OverwriteDefault'] . '\') . "';
    }
    else {
      $sql = '" . funcData::nz($GLOBALS[\'app.db\']->realEscapeString(' . ($str == 'true' ? 'funcString::utf8ToLatin($this->' . $var['Var'] . ')' : '$this->' . $var['Var']) . '), ' . $acceptsNull . ', ' . $str . ') . "';
    }
    return $sql;
  }

  private function fieldNameClean($fieldName) {
    if (strpos($fieldName, '_') == 2) {
      $fieldName = substr($fieldName, 3);
    }
    $fieldName = str_replace('_', ' ', $fieldName);
    if (strtolower($fieldName) == $fieldName) {
      //assume that if not lowercase in DB, a specific case was chosen
      $fieldName = ucwords($fieldName);
    }
    $fieldName = str_replace(' ', '', $fieldName);
    return $fieldName;
  }

  private function classNameClean($className) {
    $keywords = array(
      '__halt_compiler',
      'abstract',
      'and',
      'array',
      'as',
      'break',
      'callable',
      'case',
      'catch',
      'class',
      'clone',
      'const',
      'continue',
      'declare',
      'default',
      'die',
      'do',
      'echo',
      'else',
      'elseif',
      'empty',
      'enddeclare',
      'endfor',
      'endforeach',
      'endif',
      'endswitch',
      'endwhile',
      'eval',
      'exit',
      'extends',
      'final',
      'for',
      'foreach',
      'function',
      'global',
      'goto',
      'if',
      'implements',
      'include',
      'include_once',
      'instanceof',
      'insteadof',
      'interface',
      'isset',
      'list',
      'namespace',
      'new',
      'or',
      'print',
      'private',
      'protected',
      'public',
      'require',
      'require_once',
      'return',
      'static',
      'switch',
      'throw',
      'trait',
      'try',
      'unset',
      'use',
      'var',
      'while',
      'xor'
    );
    //$className = strtolower($className);
    $className = (substr($className, 0, 3) == 'tbl' || substr($className, 0, 3) == 'qry') ? substr($className, 3) : $className;
    $className = str_replace('_', ' ', $className);
    $className = ucwords($className);
    $className = str_replace(' ', '', $className);
    //$className = (substr($className,strlen($className)-1) == 's') ? substr($className, 0, strlen($className)-1) : $className;
    if (in_array(strtolower($className), $keywords)) {
      $className = self::classNameClean('c' . $className);
    }
    return $className;
  }

  private function classNameShort($className) {
    $className = strtolower($className);
    $className = (substr($className, 0, 3) == 'tbl' || substr($className, 0, 3) == 'qry') ? substr($className, 4) : $className;
    $className = str_replace('_', ' ', $className);
    $className = str_replace(' ', '', $className);
    $className = substr($className, 0, 1);
    return $className;
  }

  public function generateClass() {

    $output = '';
    $this->outputExtendedClass = '<?php
class ' . $this->className . ' extends _' . $this->className . ' {
  
}
?>';
    $output .= '<?php
require_once($GLOBALS[\'app.folder.include\'] . \'class.db.mysql.php\');
$filename = $GLOBALS[\'app.folder.include.extend\'] . \'class.' . $this->fileName . '.extend.php\';

if (is_file($filename)) {
  require_once($filename);
}
else {
  class ' . $this->className . ' extends _' . $this->className . ' {
  }
}

class _' . $this->className . ' {
' . $this->varFields . '
  public function __construct($' . $this->primaryKey['Var'] . ' = null) {
    if (!is_null($' . $this->primaryKey['Var'] . ')) {
      $this->lookup($' . $this->primaryKey['Var'] . ');
    }
  }

  private function getPrimaryKey($classOrDB = \'class\') {
    if (strtolower($classOrDB) == \'class\') {
      return \'' . $this->primaryKey['Var'] . '\';
    }
    elseif (strtolower($classOrDB) == \'db\') {
      return \'' . $this->primaryKey['Field'] . '\';
    }
    return null;
  }
';

    if (is_array($this->primaryKeys) && count($this->primaryKeys) > 1) {
      $param = null;
      $where = null;
      $checkNulls = null;
      $escapeStrings = null;
      foreach ($this->primaryKeys as $pk) {
        $param .= '$' . $pk['Var'] . ' = null, ';
        $where .= '
`' . $pk['Field'] . '` = \'" . $GLOBALS[\'app.db\']->realEscapeString($this->' . $pk['Var'] . ') . "\' AND';
        $checkNulls .= '
    if (empty($' . $pk['Var'] . ')) {
      return false;
    }';
        $escapeStrings .= '
    $' . $pk['Var'] . ' = (!empty($' . $pk['Var'] . ')) ? $GLOBALS[\'app.db\']->realEscapeString($' . $pk['Var'] . ') : $this->' . $pk['Var'] . ';';
      }
      $param = substr($param, 0, strlen($param) - 2);
      $where = substr($where, 0, strlen($where) - 4);
      $where .= '";';
    }
    else {
      $param = '$' . $this->primaryKey['Var'] . ' = null';
      $where = '
`' . $this->primaryKey['Field'] . '` = \'" . $GLOBALS[\'app.db\']->realEscapeString($' . $this->primaryKey['Var'] . ') . "\'";
';
      $checkNulls = '
    if (empty($' . $this->primaryKey['Var'] . ')) {
      return false;
    }
';
      $escapeStrings = '
    $' . $this->primaryKey['Var'] . ' = (!empty($' . $this->primaryKey['Var'] . ')) ? $GLOBALS[\'app.db\']->realEscapeString($' . $this->primaryKey['Var'] . ') : $this->' . $this->primaryKey['Var'] . ';';
    }
    $output .= '
  public function exists(' . $param . ') {';
    $output .= $escapeStrings;
    $output .= $checkNulls;
    $output .= '
    $sql = "SELECT `' . $this->primaryKey['Field'] . '` FROM ' . $this->table . ' WHERE ';
    $output .= $where;
    $output .= '
    $rows = $GLOBALS[\'app.db\']->executeQuery($sql, true);
    $result = (isset($rows) && $rows) ? true : false;
    return $result;
  }

  public function lookup($value = null) {
    if (!is_array($value)) {
      $value = (!is_null($value)) ? $GLOBALS[\'app.db\']->realEscapeString($value) : $this->' . $this->primaryKey['Var'] . ';
      if (is_null($value) || $value == \'\') {
        return false;
      }
      $field = \'' . $this->primaryKey['Field'] . '\';
      $value = array($field => $value);
    }
    elseif (count($value) == 0) {
      return false;
    }

    $where = array();
    foreach ($value as $field => $v) {
      $field = $GLOBALS[\'app.db\']->realEscapeString($field);
      if (is_null($v)) {
        $v = \'IS NULL\';
      }
      else {
        $v = \'= \\\'\' . $GLOBALS[\'app.db\']->realEscapeString($v) . \'\\\'\';
      }
      $where[] = "`$field` $v";
    }
    $where = implode(\' AND \', $where);

    $sql = "SELECT * FROM ' . $this->table . ' WHERE $where LIMIT 1";
    $rows = $GLOBALS[\'app.db\']->executeQuery($sql, true);
    if (isset($rows) && $rows) {
      $this->populate($rows[0]);
      return true;
    }
    return false;
  }

  static function get($select = null, $where = null, $order = null, $join = null, $page = null, $rpp = null, $countOnly = false, $sqlOnly = false, $arrayInsteadOfClass = false) {
    $select = (empty($select)) ? \'`' . $this->table . '`.*\' : $select;
    $where = (empty($where)) ? \'\' : "WHERE $where";
    $order = (empty($order)) ? \'\' : \'ORDER BY \' . $order;
    $join = (empty($join)) ? \'\' : $join;
    $page = (empty($page)) ? 1 : abs((int)$page);
    $limit = (is_numeric($rpp)) ? \'LIMIT \' . ($page - 1) * $rpp . \',\' . $rpp : \'\';
    if ($countOnly) {
      $sql = "SELECT COUNT(DISTINCT `' . $this->table . '`.`' . $this->primaryKey['Field'] . '`) AS `Total` FROM ' . $this->table . ' $join $where";
    }
    else {
      $sql = "SELECT $select FROM ' . $this->table . ' $join $where $order $limit";
    }
    if ($sqlOnly) {
      return $sql;
    }
    $rows = $GLOBALS[\'app.db\']->executeQuery($sql, true);
    if (isset($rows) && $rows) {
      if ($arrayInsteadOfClass) {
        return $rows;
      }
      if ($countOnly) {
        return $rows[0][\'Total\'];
      }
      $result = Array();
      foreach ($rows as $row) {
        $' . $this->ClassNameShort($this->className) . ' = new ' . $this->className . '();
        $' . $this->ClassNameShort($this->className) . '->populate($row);
        $result[' . (($this->primaryKeyMulti) ? '' : '$' . $this->ClassNameShort($this->className) . '->' . $this->primaryKey['Var']) . '] = $' . $this->classNameShort($this->className) . ';
      }
      return $result;
    }
    return null;
  }

  static function count($where = null, $join = null) {
    return self::get(null, $where, null, $join, null, null, true);
  }

  static function sql($select = null, $where = null, $order = null, $join = null, $page = null, $rpp = null, $countOnly = null) {
    return self::get($select, $where, $order, $join, $page, $rpp, $countOnly, true);
  }

  function populate($row) {
' . $this->populateFields . '  }

  public function save() {
    if ($this->exists()) {
      $sql = "UPDATE ' . $this->table . ' SET
' . $this->saveFields . '
WHERE';

    if (is_array($this->primaryKeys) && count($this->primaryKeys) > 1) {
      foreach ($this->primaryKeys as $pk) {
        $output .= '
`' . $pk['Field'] . '` = \'" . $GLOBALS[\'app.db\']->realEscapeString($this->' . $pk['Var'] . ') . "\' AND';
      }
      $output = substr($output, 0, strlen($output) - 4);
      $output .= '";
';
    }
    else {
      $output .= '
`' . $this->primaryKey['Field'] . '` = \'" . $GLOBALS[\'app.db\']->realEscapeString($this->' . $this->primaryKey['Var'] . ') . "\'";
';
    }

    $output .= '    }
    else {
      $sql = "INSERT INTO ' . $this->table . ' SET
' . $this->saveFields . '";
    }
    $result = $GLOBALS[\'app.db\']->executeSQL($sql);
    $result = ($result == 1) ? true : false;
    $this->' . $this->primaryKey['Var'] . ' = (empty($this->' . $this->primaryKey['Var'] . ') && $result) ? $GLOBALS[\'app.db\']->lastInsertId() : $this->' . $this->primaryKey['Var'] . ';
    return $result;
  }

  public function delete($' . $this->primaryKey['Var'] . ' = null) {
    $' . $this->primaryKey['Var'] . ' = (!empty($' . $this->primaryKey['Var'] . ')) ? $GLOBALS[\'app.db\']->realEscapeString($' . $this->primaryKey['Var'] . ') : $this->' . $this->primaryKey['Var'] . ';
    if (empty($' . $this->primaryKey['Var'] . ')) {
      return false;
    }
    $sql = "DELETE FROM ' . $this->table . ' WHERE `' . $this->primaryKey['Field'] . '` = \'$' . $this->primaryKey['Var'] . '\' LIMIT 1";
    $result = $GLOBALS[\'app.db\']->executeSQL($sql);
    return ($result == 1) ? true : false;
  }

}
?>';

    $this->output = $output;
    return true;
  }
}
?>