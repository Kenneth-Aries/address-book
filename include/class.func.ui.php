<?php
class funcUI {
  
  /**
   * $scriptType = 'js' or 'css'
   * $position = 'top' or 'bottom'
   * $source = 'embed' or 'ext'
   * $script = if $source == 'embed' then this is the actual script - will put correct tags around it
   * $script = if $source == 'ext' then relative or absolute file name to include
   * will not add a duplicate script (exact match on $script) but will return true on duplicate
   * $priority (numeric) sets the key of the array - if it is taken already, it will increase the priority of the number until an empty spot is found
   * returns bool
   */  
  static function queueScript($scriptType, $position, $source, $script, $priority = null) {
    if (!in_array($scriptType, array('js', 'css'))) {
      return false;
    }
    if (!in_array($position, array('top', 'bottom'))) {
      return false;
    }
    if (!in_array($source, array('embed', 'ext'))) {
      return false;
    }
    if (!isset($GLOBALS['app.ui.scripts'])) {
      $GLOBALS['app.ui.scripts'] = null;
    }
    if (!isset($GLOBALS['app.ui.scripts'][$scriptType]['top']) || !is_array($GLOBALS['app.ui.scripts'][$scriptType]['top'])) {
        $GLOBALS['app.ui.scripts'][$scriptType]['top'] = array();
    }
    if (!isset($GLOBALS['app.ui.scripts'][$scriptType]['bottom']) || !is_array($GLOBALS['app.ui.scripts'][$scriptType]['bottom'])) {
        $GLOBALS['app.ui.scripts'][$scriptType]['bottom'] = array();
    }
    if (!isset($GLOBALS['app.ui.scripts'][$scriptType]['top'][$source]) || !is_array($GLOBALS['app.ui.scripts'][$scriptType]['top'][$source])) {
        $GLOBALS['app.ui.scripts'][$scriptType]['top'][$source] = array();
    }
    if (!isset($GLOBALS['app.ui.scripts'][$scriptType]['bottom'][$source]) || !is_array($GLOBALS['app.ui.scripts'][$scriptType]['bottom'][$source])) {
        $GLOBALS['app.ui.scripts'][$scriptType]['bottom'][$source] = array();
    }
    if (!in_array($script, array_merge((is_array($GLOBALS['app.ui.scripts'][$scriptType]['top'][$source]) ? $GLOBALS['app.ui.scripts'][$scriptType]['top'][$source] : array()), (is_array($GLOBALS['app.ui.scripts'][$scriptType]['bottom'][$source]) ? $GLOBALS['app.ui.scripts'][$scriptType]['bottom'][$source] : array())))) {
      //do not allow duplicate scripts
      if (is_null($priority) || !is_numeric($priority) || $priority < 0) {
        $priority = 0;
      }
      while (isset($GLOBALS['app.ui.scripts'][$scriptType][$position][$source][$priority])) {
        $priority ++;
      }
      $GLOBALS['app.ui.scripts'][$scriptType][$position][$source][$priority] = $script;
      ksort($GLOBALS['app.ui.scripts'][$scriptType][$position][$source], SORT_NUMERIC);
    }
    return true;
  }

  /**
   * $scriptType = 'js' or 'css'
   * $position = 'top' or 'bottom'
   * $source = 'embed' or 'ext'
   * returns bool
   * stores array in $GLOBALS['app.ui.scripts']
   */  
  static function getExternalSourceFiles($scriptType, $position, $source) {
    if (!in_array($scriptType, array('js', 'css'))) {
      return false;
    }
    if (!in_array($position, array('top', 'bottom'))) {
      return false;
    }
    if (!in_array($source, array('embed', 'ext'))) {
      return false;
    }
    $output = null;
    if (isset($GLOBALS['app.ui.scripts'][$scriptType][$position][$source])
        && is_array($GLOBALS['app.ui.scripts'][$scriptType][$position][$source])
        && count($GLOBALS['app.ui.scripts'][$scriptType][$position][$source])) {
      if ($scriptType == 'js' && $source == 'embed') {
        $output .= '<script type="text/javascript">';
      }
      elseif ($scriptType == 'css' && $source == 'embed') {
        $output .= '<style type="text/css">';
      }
      foreach ($GLOBALS['app.ui.scripts'][$scriptType][$position][$source] as $item) {
        if ($scriptType == 'js') {
          if ($source == 'ext') {
            $output .= '<script type="text/javascript" src="' . $item . '"></script>' . "\n";
          }
          else {
            $output .= $item . "\n";
          }
        }
        elseif ($scriptType == 'css') {
          if ($source == 'ext') {
            $output .= '<link rel="stylesheet" type="text/css" href="' . $item . '" />' . "\n";
          }
          else {
            $output .= $item . "\n";
          }
        }
      } //end foreach
      if ($scriptType == 'js' && $source == 'embed') {
        $output .= '</script>';
      }
      elseif ($scriptType == 'css' && $source == 'embed') {
        $output .= '</style>';
      }
    }
    return $output;
  }

  /**
   * A single user interface
   * includes file at: $GLOBALS['app.folder.template'] . 'page/' . $class . '/' . $name;
   * file should populate variable $content
   * returns $content, or false on error
   */
  static function getPage($name, $class=null) {
    if (empty($name)) {
      return false;
    }
    $file = $GLOBALS['app.folder.template'] . 'page/' . ((is_null($class) || $class=='') ? '' : $class . '/') . $name;
    if (file_exists($file)) {
      //include the file
      global $menu;
      global $module;
      global $action;
      global $method;
      $content = null;
      include($file);
      return $content;
    }
    return false;
  }
  
  /**
   * A single response to user input
   * includes file at: $GLOBALS['app.folder.template'] . 'script/' . $class . '/' . $name;
   * file should populate variable $content OR redirect() to a confirmation page
   * returns $content, or false on error
   */
  static function runScript($name, $class=null) {
    if (empty($class) && empty($name)) {
      return false;
    }
    $file = $GLOBALS['app.folder.template'] . 'script/' . ((is_null($class) || $class=='') ? '' : $class . '/') . $name;
    if (file_exists($file)) {
      //include the file
      global $menu;
      global $module;
      global $action;
      global $method;
      $content = null;
      include($file);
      return $content;
    }
    return false;
  }
  
  /**
   * Final output
   * utilises `global $content;`
   * includes file at: $GLOBALS['app.folder.template'] . 'output/' . $theme . '/' . $name;
   * $title adds to $GLOBAL['app.title']
   * returns true, or false on error
   */
  static function renderOutput($theme, $name, $title = null) {
    if (empty($theme) || empty($name)) {
      return false;
    }
    $file = $GLOBALS['app.folder.template'] . 'output/' . $theme . '/' . $name;
    if (file_exists($file)) {
      //include the file
      global $menu;
      global $content;
      global $module;
      global $action;
      global $method;
      $content = funcAlert::get() . $content;
      funcAlert::clear();
      $cssTop = self::getExternalSourceFiles('css', 'top', 'ext');
      $cssBottom = self::getExternalSourceFiles('css', 'bottom', 'ext');
      $styleTop = self::getExternalSourceFiles('css', 'top', 'embed');
      $styleBottom = self::getExternalSourceFiles('css', 'bottom', 'embed');

      $jsTop = self::getExternalSourceFiles('js', 'top', 'ext');
      $jsBottom = self::getExternalSourceFiles('js', 'bottom', 'ext');
      $scriptTop = self::getExternalSourceFiles('js', 'top', 'embed');
      $scriptBottom = self::getExternalSourceFiles('js', 'bottom', 'embed');
      /** ::BEGIN:: This is only included in the final output if the application is set to profile **/
      if (($GLOBALS['app.profile'] && !(isset($_REQUEST['ajax']))) || ($GLOBALS['app.profile.ajax'] && (isset($_REQUEST['ajax'])))) {
        $sqlDivId = 'traceDB' . round(rand(0, 10000)); //for multiple per page (ajax query)
        $GLOBALS['app.timer.end'] = microtime(true);
        $GLOBALS['app.timer.duration'] = $GLOBALS['app.timer.end'] - $GLOBALS['app.timer.start'];
        $content .= '
<div style="clear:both"></div><br />&nbsp;<hr /><strong>script time:</strong> ' . sprintf('%f', $GLOBALS['app.timer.duration']) . '<br />
<a href="#" onclick="if (document.getElementById(\'' . $sqlDivId . '\').style.display==\'none\') {document.getElementById(\'' . $sqlDivId . '\').style.display=\'\';} else {document.getElementById(\'' . $sqlDivId . '\').style.display=\'none\';}return false;"><strong>number db queries:</strong></a> ' . count($GLOBALS['app.db.profile']) . '<br />
';
        $totalTime = 0;
        $sqlQueries = '';
        if (count($GLOBALS['app.db.profile'])) {
          $count = 0;
          foreach ($GLOBALS['app.db.profile'] as $sql) {
            $count ++;
            $totalTime += $sql['total'];
            if ($GLOBALS['app.profile.SQL'] && $GLOBALS['app.profile.outputSQL']) {
              $sqlQueries .= '<tr>
                                <td align="right">'.$count.'</td>
                                <td valign="top" align="right">'.$sql['count'].'</td>
                                <td valign="top">'.(isset($sql['cache']) ? $sql['cache'] : null).'</td>
                                <td valign="top">'.htmlspecialchars($sql['sql']).'</td>
                                <td valign="top" align="right">'.sprintf('%f', $sql['query']).'</td>
                                <td valign="top" align="right">'.sprintf('%f', $sql['fetch']).'</td>
                                <td valign="top" align="right">'.sprintf('%f', $sql['total']).'</td>
                            </tr>';
            }
          }
        }
        $content .= '<strong>mysql time:</strong> ' . sprintf('%f', $totalTime) . '<br />
<strong>memory:</strong> '.  (round(memory_get_usage(true)/1024, 2)) . ' kb' . '<br />
<div id="' . $sqlDivId . '" style="display:none">
<table border="1">
<tr>
    <td>#</td>
    <td>count</td>
    <td>cache</td>
    <td>sql</td>
    <td>query</td>
    <td>fetch</td>
    <td>total</td>
</tr>
'.$sqlQueries.'</table>
</div>'."\n";
      }
      /** ::END:: This is only included in the final output if the application is set to profile **/
      include($file);
      echo funcAlert::get(); //check for alerts generated during the render
      exit();
    }
    exit();
  }
  
}
?>