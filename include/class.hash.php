<?php
class Hash {
  var $MailId = null;
  var $TargetId = null;
  var $BrandId = null;
  var $LinkId = null;
  var $isEmailAddr = false;
  var $Hash = null;
  var $Error = true;
  var $ErrorDesc = null;
  const codesetMail = "0123456789abcdefghijkmnopqrstuvwxyz";
  const codesetLink = "0123456789abcdefghijkmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
  const shaseed = "902ba3cda1883801594b6e1b452790cc53948fda";

  public function __construct() {
  }

  public function Encode($MailId = -1, $TargetId = -1, $BrandId = -1, $LinkId = 0, $isEmailAddr = false) {
    $this->MailId = ($MailId == -1) ? $this->MailId : $MailId;
    $this->TargetId = ($TargetId == -1) ? $this->TargetId : $TargetId;
    $this->BrandId = ($BrandId == -1) ? $this->BrandId : $BrandId;
    $this->LinkId = ($LinkId == -1) ? $this->LinkId : $LinkId;
    $this->isEmailAddr = $isEmailAddr;
    if (!$this->varsOK()) {
      $this->Error = true;
      $this->ErrorDesc = 'Not all parameters given for encoding';
      $this->Hash = NULL;
      return false;
    }
    else {
      $this->Hash  = '';
      $this->Hash .= substr((sha1($this->MailId+$this->TargetId+$this->BrandId+$this->LinkId).self::shaseed),6,1);
      $this->Hash .= $this->internalEncode(($this->MailId + 10).$this->vhCalcsum($this->MailId + 10));
      $this->Hash .= 'l';
      $this->Hash .= $this->internalEncode(($this->TargetId + 10).$this->vhCalcsum(($this->TargetId + 10)));
      $this->Hash .= 'l';
      $this->Hash .= $this->internalEncode(($this->BrandId + 10).$this->vhCalcsum($this->BrandId + 10));
      $this->Hash .= 'l';
      $this->Hash .= $this->internalEncode(($this->LinkId + 10).$this->vhCalcsum(($this->LinkId + 10)));
      $this->Hash .= substr(sha1($this->internalEncode($this->vhCalcsum($this->MailId+$this->TargetId+$this->BrandId+$this->LinkId)).self::shaseed),10,1);
      $this->Error = false;
      $this->ErrorDesc = null;
    }
  }
    
  public function Decode($Hash = -1, $isEmailAddr = false) {
    $this->Hash = ($Hash == -1) ? $this->Hash : $Hash;
    $this->isEmailAddr = $isEmailAddr;
    $params = explode('l',substr($this->Hash,1,strlen($this->Hash)-2));
    if (count($params) != 4) {
      $this->Error = true;
      $this->ErrorDesc = 'Integrety check of hash failed (not all parts found)';
      $this->clearIdVars();
      return false;
    }
    $mailIdWithChk = $this->internalDecode($params[0]);
    $targetIdWithChk = $this->internalDecode($params[1]);
    $brandIdWithChk = $this->internalDecode($params[2]);
    $linkIdWithChk = $this->internalDecode($params[3]);
    $this->MailId = ($this->vhChecksum($mailIdWithChk)) ? floor($mailIdWithChk/10) -10  : -1;
    $this->TargetId = ($this->vhChecksum($targetIdWithChk)) ? floor($targetIdWithChk/10) -10 : -1;
    $this->BrandId = ($this->vhChecksum($brandIdWithChk)) ? floor($brandIdWithChk/10) -10 : -1;
    $this->LinkId = ($this->vhChecksum($linkIdWithChk)) ? floor($linkIdWithChk/10) -10 : -1;
    if (!$this->varsOK()) {
      $this->Error = true;
      $this->ErrorDesc = 'Check digit incorrect';
      $this->clearIdVars();
      return false;
    }
    if (substr((sha1($this->MailId+$this->TargetId+$this->BrandId+$this->LinkId).self::shaseed),6,1) != substr($this->Hash,0,1) ||
        substr(sha1($this->internalEncode($this->vhCalcsum($this->MailId+$this->TargetId+$this->BrandId+$this->LinkId)).self::shaseed),10,1) != substr($this->Hash,-1)) {
      $this->Error = true;
      $this->ErrorDesc = 'Seed Parameters incorrect';
      $this->clearIdVars();
      return false;
    }
    $this->Error = false;
    return true;
  }

  private function internalEncode($n) {
    $codeset = ($this->isEmailAddr) ? self::codesetMail : self::codesetLink;
    $base = strlen($codeset);
    $converted = '';
    while ($n > 0) {
      $converted = substr($codeset, bcmod($n,$base), 1) . $converted;
      $n = self::bcFloor(bcdiv($n, $base));
    }
    return $converted;
  }

  private function internalDecode($hash) {
    $codeset = ($this->isEmailAddr) ? self::codesetMail : self::codesetLink;
    $base = strlen($codeset);
    $c = '0';
    for ($i = strlen($hash); $i; $i--) {
      $c = bcadd($c,bcmul(strpos($codeset, substr($hash, (-1 * ( $i - strlen($hash) )),1)), bcpow($base,$i-1)));
    }
    return bcmul($c, 1, 0);
  }

  static private function bcFloor($x) {
    return bcmul($x, '1', 0);
  }

  static private function bcCeil($x) {
    $floor = bcFloor($x);
    return bcadd($floor, ceil(bcsub($x, $floor)));
  }

  static private function bcRound($x) {
    $floor = bcFloor($x);
    return bcadd($floor, round(bcsub($x, $floor)));
  }
    
  private function varsOK() {
    if (!is_numeric($this->MailId) || $this->MailId == -1) return false;
    if (!is_numeric($this->TargetId) || $this->TargetId == -1) return false;
    if (!is_numeric($this->BrandId) || $this->BrandId == -1) return false;
    if (!is_numeric($this->LinkId) || $this->LinkId == -1) return false;
    if (!isset($this->isEmailAddr)) return false;
    return true;
  }
    
  private function clearIdVars() {
    $this->MailId = null;
    $this->TargetId = null;
    $this->BrandId = null;
    $this->LinkId = null;
  }
    
  private static function vhD($j, $k) {
  	$table = array(
  		array(0,1,2,3,4,5,6,7,8,9),
  		array(1,2,3,4,0,6,7,8,9,5),
  		array(2,3,4,0,1,7,8,9,5,6),
  		array(3,4,0,1,2,8,9,5,6,7),
  		array(4,0,1,2,3,9,5,6,7,8),
  		array(5,9,8,7,6,0,4,3,2,1),
  		array(6,5,9,8,7,1,0,4,3,2),
  		array(7,6,5,9,8,2,1,0,4,3),
  		array(8,7,6,5,9,3,2,1,0,4),
  		array(9,8,7,6,5,4,3,2,1,0),
		);
	
	 return $table[$j][$k];
  }

  private static function vhP($pos, $num) {
  	$table = array(
  		array(0,1,2,3,4,5,6,7,8,9),
  		array(1,5,7,6,2,8,3,0,9,4),
  		array(5,8,0,3,7,9,6,1,4,2),
  		array(8,9,1,6,0,4,3,5,2,7),
  		array(9,4,5,3,1,2,6,8,7,0),
  		array(4,2,8,6,5,7,3,9,0,1),
  		array(2,7,9,3,8,0,6,4,1,5),
  		array(7,0,4,6,9,1,3,2,5,8),
		);
	
  	return $table[$pos % 8][$num];
  }

  private static function vhInv($j) {
  	$table = array(0,4,3,2,1,5,6,7,8,9);
    return $table[$j];
  }

  private static function vhCalcsum($number) {
  	$c = 0;
  	$n = strrev($number);
  
  	$len = strlen($n);
  	for ($i = 0; $i < $len; $i++)
  		$c = self::vhD($c, self::vhP($i+1, $n[$i]));
  
  	return self::vhInv($c);
  }

  private static function vhChecksum($number) {
  	$c = 0;
  	$n = strrev($number);
  
  	$len = strlen($n);
  	for ($i = 0; $i < $len; $i++)
  		$c = self::vhD($c, self::vhP($i, $n[$i]));
  
  	return ($c == 0) ? true : false;
  }
}

class HashEncode extends Hash {
  public function __construct($MailId, $TargetId, $BrandId, $LinkId = 0, $isEmailAddr = false) {
    $this->Encode($MailId, $TargetId, $BrandId, $LinkId, $isEmailAddr);
  }
}

class HashDecode extends Hash {
  public function __construct($Hash, $isEmailAddr = false) {
    $this->Decode($Hash, $isEmailAddr);
  }
}
?>