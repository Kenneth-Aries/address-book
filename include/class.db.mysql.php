<?php
class db {
  private $server;
  private $username;
  private $password;
  private $database;
  private $connection;
  private $errorMessage;

  public $connected = false;

  public function __construct($server='', $username='', $password='', $database='') {
    if (!empty($server) && !empty($username) && !empty($password) && !empty($database)) {
      $this->server = $server;
      $this->username = $username;
      $this->password = $password;
      $this->database = $database;
      $this->connect();
    }
  }

  public function connect() {
    if (!$this->connected) {
      $this->connection = @mysqli_connect($this->server, $this->username, $this->password);
      if ($this->connection) {
        if (mysqli_select_db($this->connection, $this->database)) {
          $this->connected = true;
          $this->errorMessage = null;
        }
        else {
          $this->errorMessage = mysqli_error($this->connection);
          if ($GLOBALS['app.db.echoErrors']) {
            funcAlert::add(mysqli_error($this->connection), $GLOBALS['app.alert.error']);
          }
        }
      }
      else {
        $this->errorMessage = mysqli_error($this->connection);
        if ($GLOBALS['app.db.echoErrors']) {
          funcAlert::add(mysqli_error($this->connection), $GLOBALS['app.alert.error']);
        }
      }
    }
  }

  public function disconnect() {
    if ($this->connected) {
      mysqli_close($this->connection);
      $this->connected = false;
    }
  }

  public function getServerLoad() {
    $response = funcNet::httpCurl('http://freya/load.php');
    if ($response['http_code'] == 200) {
      return $response['body'];
    }
  }

  public function isServerLoadOK($threshold = 5) {
    $loadInfo = json_decode(self::getServerLoad());
    if (isset($loadInfo[0])) {
      $load = (float)$loadInfo[0];
      if ($load > $threshold) {
        return 'DB server load too high: ' . $load . '. Threshold: ' . $threshold;
      }
    }
    return true;
  }

  public function executeQuery($sql, $namesOnly = true) {
    if (isset($GLOBALS['app.db.usecache']) && $GLOBALS['app.db.usecache']) {
      $cacheFolder = $GLOBALS['app.folder.cache.sql'];
      if (!file_exists($cacheFolder)) {
        mkdir($cacheFolder);
      }
      $cacheFile = $cacheFolder . sha1($sql);
      if (file_exists($cacheFile)) {
        if (strtotime('now') - filemtime($cacheFile) <= $GLOBALS['app.db.cachetime']) {
          $result = unserialize(funcFS::readFile($cacheFile));
          if (!empty($result)) {
            if ($GLOBALS['app.profile.SQL']) {
              $GLOBALS['app.db.profile'][] = array('sql' => $sql, 'query' => 0, 'fetch' => 0, 'total' => 0, 'count' => count($result), 'cache' => 'HIT'); //, 'trace' => debug_backtrace()
            }
            return $result;
          }
        }
        else {
          unlink($cacheFile);
        }
      }
    }
    $GLOBALS['app.db.lastSQL'] = $sql;
    // used for sql that returns a recordset
    if ($this->connected) {
      $tmpStart = microtime(true);
      $result = mysqli_query($this->connection, $sql);
      $tmpQuery = microtime(true);
      if (isset($result)) {
        if (!$result || mysqli_errno($this->connection) != 0) {
          $this->errorMessage = mysqli_error($this->connection);
          if ($GLOBALS['app.db.echoErrors']) {
            funcAlert::add(mysqli_error($this->connection).($GLOBALS['app.db.echoSQL'] ? '<br /><br /><b>SQL:</b> '.$sql : ''), $GLOBALS['app.alert.error']);
          }
          return false;
        }
        $array = array();
        while ($row = mysqli_fetch_array($result, ($namesOnly ? MYSQLI_ASSOC : MYSQLI_BOTH))) {
          $array[] = $row;
        }
        $tmpEnd = microtime(true);
        if ($GLOBALS['app.profile.SQL']) {
          $GLOBALS['app.db.profile'][] = array('sql' => $sql, 'query' => ($tmpQuery - $tmpStart), 'fetch' => ($tmpEnd - $tmpQuery), 'total' => ($tmpEnd - $tmpStart), 'count' => count($array), 'cache' => 'MISS'); //, 'trace' => debug_backtrace()
        }
        if (isset($GLOBALS['app.db.usecache']) && $GLOBALS['app.db.usecache']) {
          funcFS::writeFile($cacheFile, serialize($array));
        }
        return $array;
      }
      else {
        if (mysqli_errno($this->connection) != 0) {
          $this->errorMessage = mysqli_error($this->connection);
          if ($GLOBALS['app.db.echoErrors']) {
            funcAlert::add(mysqli_error($this->connection).($GLOBALS['app.db.echoSQL'] ? '<br /><b>SQL:</b> '.$sql : ''), $GLOBALS['app.alert.error']);
          }
          return false;
        }
      }
    }
    return false;
  }
  
  public function getDatabases() {
    if (isset($GLOBALS['app.db.dbs'])) {
      //return cached result
      return $GLOBALS['app.db.dbs'];
    }
    $rows = $this->executeQuery('SHOW DATABASES;', false);
    if (isset($rows) && $rows) {
      $array = Array();
      foreach ($rows as $row) {
        $array[] = $row[0];
      }
      //cache result
      $GLOBALS['app.db.dbs'] = $array;
      return $array;
    }
    return null;
  }

  public function getTables($database) {
    if (isset($GLOBALS['app.db.' . $database . '.tables'])) {
      //return cached result
      return $GLOBALS['app.db.' . $database . '.tables'];
    }
    $rows = $this->executeQuery("SHOW TABLES FROM `$database`;", false);
    if (isset($rows) && $rows) {
      $array = Array();
      foreach ($rows as $row) {
        $array[] = $row[0];
      }
      //cache result
      $GLOBALS['app.db.' . $database . '.tables'] = $array;
      return $array;
    }
    return null;
  }

  public function executeSQL($sql) {
    $GLOBALS['app.db.lastSQL'] = $sql;
    // used for sql that doesn't expect a recordset back
    if ($this->connected) {
      $tmpStart = microtime(true);
      $result = mysqli_query($this->connection, $sql);
      if (!$result || mysqli_errno($this->connection) != 0) {
        $this->errorMessage = mysqli_error($this->connection);
        if ($GLOBALS['app.db.echoErrors']) {
          funcAlert::add(mysqli_error($this->connection).($GLOBALS['app.db.echoSQL'] ? '<br /><br /><b>SQL:</b> '.$sql : ''), $GLOBALS['app.alert.error']);
        }
        return false;
      }
      $tmpEnd = microtime(true);
      if ($GLOBALS['app.profile.SQL']) {
        $GLOBALS['app.db.profile'][] = array('sql' => $sql, 'query' => ($tmpEnd - $tmpStart), 'fetch' => 0, 'total' => ($tmpEnd - $tmpStart), 'count' => mysqli_affected_rows($this->connection)); //, 'trace' => debug_backtrace()
      }
      return $result;
    }
    return false;
  }

  public function lastInsertId() {
    if ($this->connected) {
      return mysqli_insert_id($this->connection);
    }
    return null;
  }

  public function realEscapeString($string) {
    if ($this->connected) {
      return mysqli_real_escape_string($this->connection, (string) $string);
    }
    return null;
  }
}
?>