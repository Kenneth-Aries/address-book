<?php
class Pagination {
  var $page;
  var $rpp;
  protected $pageDefault = 1;
  protected $rppDefault = 10;
  protected $rppMax = 500;
  protected $pageVar = 'page';
  protected $rppVar = 'rpp';  
  protected $totalRecords = 0;
  protected $lastPage;

  function __construct($pageVar = 'page', $rppVar = 'rpp', $totalRecords = 0) {
    $this->pageVar = $pageVar;
    $this->rppVar = $rppVar;
    $this->totalRecords = $totalRecords;
    $this->page = (isset($_REQUEST[$this->pageVar]) && is_numeric($_REQUEST[$this->pageVar]) && $_REQUEST[$this->pageVar] > 0) ? $_REQUEST[$this->pageVar] : $this->pageDefault;
    $this->rpp = (isset($_REQUEST[$this->rppVar]) && is_numeric($_REQUEST[$this->rppVar]) && $_REQUEST[$this->rppVar] > 0 && $_REQUEST[$this->rppVar] < $this->rppMax) ? $_REQUEST[$this->rppVar] : $this->rppDefault;    
  }

  function getLastPage() {
    return $lastPage;
  }

  function getQueryString() {
    return "&amp;$this->pageVar=$this->page&amp;$this->rppVar=$this->rpp";
  }
  
  function generateLinks($currentLink) {
    $this->lastPage = ceil($this->totalRecords/$this->rpp);
    $this->page = ($this->page > $this->lastPage) ? $this->lastPage : $this->page; 
    $content = '';
    $currentLink .= (strpos($currentLink, '?')===false ? '?' : '&amp;')."$this->rppVar=$this->rpp";

    $content .= '
              <table align="center">
                <tr>
                  <td colspan="5" align="center">
                    (Page ' . $this->page . ' of ' . $this->lastPage . ')
                  </td>
                </tr>
                <tr align="center">';
    $content .= ($this->page <= 1) ? '
                  <td width="30"></td>
                  <td width="30"></td>' : '
                  <td width="30">
                    <div class="page_first" title="Go to first page"><a class="fillspace" href="' . $currentLink . '&amp;' . $this->pageVar . '=1"></a></div>
                  </td>
                  <td width="30">
                    <div class="page_prev" title="Previous page"><a class="fillspace" href="' . $currentLink . '&amp;' . $this->pageVar . '=' . ($this->page - 1) . '"></a></div>
                  </td>';
    $content .= '
                  <td style="padding-left:20px;padding-right:20px">';
    $start = ($this->page > 5 && $this->lastPage > 15) ? $this->page - 5 : 1;
    $stop = ($this->lastPage > ($this->page + 5) && $this->lastPage > 15) ? $this->page + 5 : $this->lastPage;
    if ($start <> $stop) {
      $content .= ($start == 1) ? '' : '...';
      for ($i=$start; $i<($stop+1); $i++) {
        $content .= ($this->page == $i) ? $i . ' ' : '<a href="' . $currentLink . '&amp;' . $this->pageVar . '=' . ($i) . '">' . $i . '</a> ';
      }
      $content .= ($stop == $this->lastPage) ? '' : '...';
    }
    $content .= '
                  </td>';
    $content .= ($this->page == $this->lastPage) ? '
                  <td width="30"></td>
                  <td width="30"></td>' : '
                  <td width="30">
                    <div class="page_next" title="Next page"><a class="fillspace" href="' . $currentLink . '&amp;' . $this->pageVar . '=' . ($this->page + 1) . '"></a></div>
                  </td>
                  <td width="30">
                    <div class="page_last" title="Go to last page"><a class="fillspace" href="' . $currentLink . '&amp;' . $this->pageVar . '=' . $this->lastPage . '"></a></div>
                  </td>';
    $content .= '
                </tr>
              </table>';
    return $content;
  }
}
?>