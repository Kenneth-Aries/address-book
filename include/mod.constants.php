<?php
/**
 * Default folders
**/
$file = __FILE__;
if (strpos($file, '/') !== false) {
  $tmp = explode('/', $file);
}
elseif (strpos($file, '\\') !== false) {
  $tmp = explode('\\', $file);
}
array_pop($tmp);
array_pop($tmp);
if (strpos($file, '/') !== false) {
  $folder = implode('/', $tmp);
}
elseif (strpos($file, '\\') !== false) {
  $folder = implode('\\', $tmp);
}
unset($file);
unset($tmp);

$GLOBALS['app.folder'] = $folder . '/';
$GLOBALS['app.folder.include'] = $GLOBALS['app.folder'] . 'include/';
$GLOBALS['app.folder.include.generic'] = $GLOBALS['app.folder.include'] . 'generic/';
$GLOBALS['app.folder.include.extend'] = $GLOBALS['app.folder.include'] . 'extend/';
$GLOBALS['app.folder.template'] = $GLOBALS['app.folder'] . 'template/';
$sessionId = session_id();
$GLOBALS['app.folder.cache.sql'] = $GLOBALS['app.folder'] . 'cache/sql/' . (empty($sessionId) ? '_cron' : $sessionId) . '/';

if (isset($_SESSION['app.alerts']) && is_array($_SESSION['app.alerts'])) {
  $GLOBALS['app.alerts'] = $_SESSION['app.alerts'];
  unset($_SESSION['app.alerts']);
}
else {
  $GLOBALS['app.alerts'] = array();
}

$GLOBALS['app.title'] = 'Address Book';
$GLOBALS['app.ui.theme'] = 'addressbook';
$GLOBALS['app.ui.theme.folder'] = 'template/output/' . $GLOBALS['app.ui.theme'] . '/';

if ($_SERVER['SERVER_ADDR']=='192.168.0.107') {
  $GLOBALS['app.devMode'] = true;
  $GLOBALS['app.profile'] = true;
  $GLOBALS['app.profile.SQL'] = true;
  $GLOBALS['app.profile.outputSQL'] = true;
  $GLOBALS['app.profile.ajax'] = false;

  $GLOBALS['app.db.host'] = 'localhost';
  $GLOBALS['app.db.db'] = 'kenneth_addressbook';
  $GLOBALS['app.db.un'] = 'dev';
  $GLOBALS['app.db.pw'] = 'itcrowd';


  $GLOBALS['app.db.echoSQL'] = true;
  $GLOBALS['app.db.echoErrors'] = true;
  $GLOBALS['app.db.usecache'] = false;
  $GLOBALS['app.db.cachetime'] = 0; //0 = within the same second

  $GLOBALS['app.mail.smtp'] = true;
  $GLOBALS['app.mail.smtp.host'] = '192.168.0.101';
  $GLOBALS['app.mail.smtp.port'] = '25';
  $GLOBALS['app.mail.smtp.un'] = '';
  $GLOBALS['app.mail.smtp.pw'] = '';
  $GLOBALS['app.mail.testEmail'] = 'rob@simdirect.co.za';
//  echo '<pre>' . print_r($GLOBALS, true) . '</pre>';
//  exit;
}
else {
  $GLOBALS['app.devMode'] = false;
  $GLOBALS['app.profile'] = false;
  $GLOBALS['app.profile.SQL'] = false;
  $GLOBALS['app.profile.outputSQL'] = false;
  $GLOBALS['app.profile.ajax'] = false;

  $GLOBALS['app.db.host'] = 'hermoor';
  $GLOBALS['app.db.db'] = 'simdirect_skeleton';
  $GLOBALS['app.db.un'] = 'dev';
  $GLOBALS['app.db.pw'] = 'itcrowd';
  $GLOBALS['app.db.echoSQL'] = true;
  $GLOBALS['app.db.echoErrors'] = true;
  $GLOBALS['app.db.usecache'] = false;
  $GLOBALS['app.db.cachetime'] = 0; //0 = within the same second

  $GLOBALS['app.mail.smtp'] = true;
  $GLOBALS['app.mail.smtp.host'] = 'localhost';
  $GLOBALS['app.mail.smtp.port'] = '25';
  $GLOBALS['app.mail.smtp.un'] = '';
  $GLOBALS['app.mail.smtp.pw'] = '';
  $GLOBALS['app.mail.testEmail'] = 'ken13p@gmail.com';

}

/**
 * class.db.mysql.php will populate this array
 * [0] => array('sql' => 'SELECT ...', 'query' => 0.032, 'fetch' => 0.001, 'total' => 0.033);
 */
$GLOBALS['app.db.profile'] = array();

/**
 * Variable global settings settings
**/
$GLOBALS['app.var.user.redirectafterlogin'] = 'home.php?module=contacts&action=list'; //redirect to this page after login
$GLOBALS['app.var.user.classname'] = ''; //if this is blank or the class doesn't exist, no users are used
$GLOBALS['app.var.user.username'] = ''; //Username for text login screen, Id for dropdown login screen
$GLOBALS['app.var.user.email'] = ''; //Email for text login screen
$GLOBALS['app.var.user.name'] = '';
$GLOBALS['app.var.user.password'] = 'Password';
$GLOBALS['app.var.user.password.encryption'] = 'sha1'; //valid values are 'sha1' or 'md5' or ''
$GLOBALS['app.var.user.password.salt'] = 's1mD1r3CT'; //appended to end of password
$GLOBALS['app.var.user.activestatus.field'] = 'Status'; //if blank, ignored
$GLOBALS['app.var.user.activestatus.active'] = 'ACTIVE';

/**
 * Alert levels
**/
$GLOBALS['app.alert.all'] = 0;
$GLOBALS['app.alert.warning'] = 1;
$GLOBALS['app.alert.'.$GLOBALS['app.alert.warning']] = 'warning';
$GLOBALS['app.alert.warning.class'] = 'ui-state-highlight';
$GLOBALS['app.alert.warning.icon'] = 'ui-icon-info';
$GLOBALS['app.alert.warning.title'] = 'Alert';

$GLOBALS['app.alert.error'] = 2;
$GLOBALS['app.alert.'.$GLOBALS['app.alert.error']] = 'error';
$GLOBALS['app.alert.error.class'] = 'ui-state-error';
$GLOBALS['app.alert.error.icon'] = 'ui-icon-alert';
$GLOBALS['app.alert.error.title'] = 'Error';

$GLOBALS['app.alert.success'] = 3;
$GLOBALS['app.alert.'.$GLOBALS['app.alert.success']] = 'success';
$GLOBALS['app.alert.success.class'] = 'ui-state-success';
$GLOBALS['app.alert.success.icon'] = 'ui-icon-check';
$GLOBALS['app.alert.success.title'] = 'Success';

?>