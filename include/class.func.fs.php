<?php
class funcFS {
  
  static function deleteFilesXSecondsOld($folder, $seconds) {
    $now = strtotime('now');
    if (is_dir($folder)) {
      if ($fh = opendir($folder)) {
        while (($file = readdir($fh)) !== false) {
          if (is_file($folder . $file) && $now >= (filemtime($folder . $file)+$seconds)) {
            $result = unlink($folder . $file);
            //echo $file . ' - ' . ($result ? 'Y' : 'N') . '<br />';
          }
        }
        closedir($fh);
      }
    }
  }
  
  static function getCSV($file) {
    $content = funcFS::readFile($file);
    if ($content) {
      $fh = fopen('php://memory', 'rw');
      fwrite($fh, $content);
      fseek($fh, 0);
      $content = array();
      while (($data = fgetcsv($fh)) !== false) {
        $content[] = $data;
      }
      fclose($fh);
    }
    return $content;
  }
  
  static function readFile($file) {
    if (is_file($file)) {
      $f = fopen($file, 'r');
      $content = fread($f, filesize($file));
      $result = fclose($f);
      return $result ? $content : $result;
    }
    return false;
  }
  
  static function getParentFolder($file) {
    $file = explode('/', $file);
    $end = array_pop($file);
    if ($end == '') {
      array_pop($file);
    }
    return implode('/', $file) . '/';
  }
  
  static function createFolder($folder) {
    if (substr($folder, -1, 1) != '/') {
      $folder .= '/';
    }
    if (!file_exists($folder)) {
      return mkdir($folder, 0755, true);
    }
    return true;
  }
  
  static function writeFile($file, $content) {
    $folder = funcFS::getParentFolder($file);
    funcFS::createFolder($folder);
    if (!file_exists($file)) {
      touch($file);
    }
    $f = fopen($file, 'w');
    $result = fwrite($f, $content);
    fclose($f);
  }
  
  static function getFilesInFolder($folder) {
    if (substr($folder, -1, 1) != '/') {
      $folder .= '/';
    }
    $files = glob($folder . '*', GLOB_MARK);
    $result = null;
    if ($files) {
      foreach ($files as $file) {
        if (is_file($file)) {
          if (!is_array($result)) {
            $result = array();
          }
          $result[] = $file;
        }
      }
    }
    return $result;
  }

  static function getSubfolders($folder) {
    if (substr($folder, -1, 1) != '/') {
      $folder .= '/';
    }
    $files = glob($folder . '*', GLOB_ONLYDIR + GLOB_MARK);
    $result = null;
    if ($files) {
      foreach ($files as $file) {
        if (is_dir($file)) {
          if (!is_array($result)) {
            $result = array();
          }
          $result[] = $file;
        }
      }
    }
    return $result;
  }

  static function delTree($folder) {
    if (substr($folder, -1, 1) != '/') {
      $folder .= '/';
    }
    $files = glob($folder . '*', GLOB_MARK);
    foreach ($files as $file) {
      if (is_dir($file)) {
        funcFS::delTree($file);
      }
      else {
        unlink($file);
      }
    }
    if (is_dir($folder)) {
      rmdir($folder);
    }
  }


}
?>