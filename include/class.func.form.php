<?php
class funcForm {
  
  static function form($name, $method = 'post', $action = null, $fileUpload = false, $customProperty = null) {
    return '<form name="'.$name.'" id="'.$name.'" method="'.$method.'" action="'.$action.'"' . ($fileUpload ? ' enctype="multipart/form-data"' : '') . (is_null($customProperty) ? null : ' ' . $customProperty) . '>';
  }

  static function closeForm() {
    return '</form>';
  }
  
  static function _sortOptionT($option1, $option2) {
    $text1 = (isset($option1['groupName']) ? $option1['groupName'] . '|' : '') . $option1['text'];
    $text2 = (isset($option2['groupName']) ? $option2['groupName'] . '|' : '') . $option2['text'];
    if ($text1 == $text2) {
      return 0;
    }
    return ($text1 < $text2) ? -1 : 1;
  }

  static function _sortOptionV($option1, $option2) {
    if ($option1['value'] == $option2['value']) {
      return 0;
    }
    return ($option1['value'] < $option2['value']) ? -1 : 1;
  }
  
  /**
   * Sorts by text by default. pass in TRUE to sort by value
   */
  static function sortOption($options, $byValue = false) {
    if (!is_array($options) || count($options) == 0) {
      return null;
    }
    if ($byValue) {
      usort($options, 'funcForm::_sortOptionV');
    }
    else {
      usort($options, 'funcForm::_sortOptionT');
    }
    return $options;
  }
  
  static function prepOption($value, $text = null, $onclick = null, $groupName = null, $tag = null) {
    if (is_null($text)) {
      $text = $value;
    }
    if (ctype_digit($value) || (is_numeric($value) && ctype_digit((string)($value*-1)))) {
      $value = (int)$value;
    }
    $arrOption = array();
    $arrOption['value'] = $value;
    $arrOption['text'] = $text;
    if (!is_null($onclick)) {
      $arrOption['onclick'] = $onclick;
    }
    if (!is_null($groupName)) {
      $arrOption['groupName'] = $groupName;
    }
    if (!is_null($tag)) {
      $arrOption['tag'] = $tag;
    }
    return $arrOption;
  }
  
  static function checkbox($name, $value = null, $checked = false, $cssClass = null, $customProperty = null) {
    return '<input type="checkbox" id="' . $name . '" name="' . $name . '" value="'.htmlentities($value).'"' . ($checked ? ' CHECKED="CHECKED"' : '') . (is_null($cssClass) ? '' : ' class="' . $cssClass . '"') . (is_null($customProperty) ? null : ' ' . $customProperty) . ' />';
  }

  static function radio($id = null, $groupName, $value = null, $checked = false, $cssClass = null, $customProperty = null) {
    return '<input type="radio" id="' . $id . '" value="'.htmlentities($value).'" name="' . $groupName . '"' . ($checked ? ' CHECKED="CHECKED"' : '') . (is_null($cssClass) ? '' : ' class="' . $cssClass . '"') . (is_null($customProperty) ? null : ' ' . $customProperty) . ' />';
  }

  static function select($name, $value = null, $pleaseSelect = null, $listData = null, $cssClass = null, $readOnly = false, $customProperty = null, $id = null) {
    if (ctype_digit($value) || (is_numeric($value) && ctype_digit((string)($value*-1)))) {
      $value = (int)$value;
    }
    $html = '<select class="'.$cssClass.'" name="'.$name.'" id="'.(is_null($id) ? $name : $id).'" ' . $customProperty . '>';
    if (!is_null($pleaseSelect)) {
      $html .= '<option value="">'.htmlspecialchars($pleaseSelect).'</option>';
    }
    $groupName = null;
    if (is_array($listData) && count($listData)>0) {
      foreach ($listData as $d) {
        if (ctype_digit($d['value']) || (is_numeric($d['value']) && ctype_digit((string)($d['value']*-1)))) {
          $d['value'] = (int)$d['value'];
        }
        if (isset($d['groupName']) && $groupName != $d['groupName']) {
          if (!is_null($groupName)) {
            $html .= '</optgroup>';
          }
          $groupName = $d['groupName'];
          $html .= '<optgroup label="' . htmlentities($groupName) . '">';
        }
        $onclick = (isset($d['onclick']) && !is_null($d['onclick'])) ? ' onclick="'.$d['onclick'].'"' : '';
        $html .= '<option value="'.$d['value'].'"'.((((!is_array($value) && $value === $d['value']) || (is_array($value) && in_array($d['value'], $value, true))) && !is_null($value)) ? ' selected' : '').$onclick.'>'.htmlspecialchars($d['text']).'</option>';
      }
    }
    if (!is_null($groupName)) {
      $html .= '</optgroup>';
    }
    $html .= '</select>';
    return $html;
  }
  
  static function listboxes($name, $value = null, $pleaseSelect = false, $listData = null, $cssClass = null, $readOnly = false, $customProperty = null) {
    $html = '<table border="0" cellpadding="3" cellspacing="0"><tr>';
    $html .= '<td><select type="text" class="' . $cssClass . '" name="' . $name . 'From" id="' . $name . 'From" ' . (is_null($customProperty) ? null : ' ' . $customProperty) . '>';
    if ($pleaseSelect !== false) {
      $html .= '<option value="">' . htmlspecialchars($pleaseSelect) . '</option>';
    }
    if (is_array($listData) && count($listData)>0) {
      foreach ($listData as $d) {
        $onclick = isset($d['onclick']) ? ' onclick="' . $d['onclick'] . '"' : '';
        $html .= '<option value="' . $d['id'] . '"' . ($value == $d['id'] ? ' selected' : null) . $onclick . '>' . htmlspecialchars($d['data']) . '</option>';
      }
    }
    $html .= '</select></td>';
    $html .= '<td>' . $this->CreateButton('btnAdd_' . $name, 'Add', 'OptionMove(dge_id(\'' . $name . 'From\'), dge_id(\'' . $name . 'To\'), dge_id(\'' . $name . 'From\').value);') . $this->CreateButton('btnRemove_' . $name, 'Remove', 'OptionMove(dge_id(\'' . $name . 'To\'), dge_id(\'' . $name . 'From\'), dge_id(\'' . $name . 'To\').value);').'</td>';
    $html .= '<td><select type="text" class="' . $cssClass . '" name="' . $name . 'To[]" id="' . $name . 'To" ' . (is_null($customProperty) ? null : ' ' . $customProperty) . ' multiple="multiple">';
    $html .= '</select>';
    $html .= '</td></tr></table>';
    return $html;
  }
  
  static function file($name, $cssClass = null, $customProperty = null) {
    return '<input type="file" class="' . $cssClass . '" name="' . $name . '" id="' . $name . '" ' . (is_null($customProperty) ? null : ' ' . $customProperty) . ' />';
  }
  
  static function text($name, $value = null, $cssClass = null, $maxLength = null, $readOnly = false, $customProperty = null) {
    return '<input type="text" class="'.$cssClass.'" name="'.$name.'" id="'.$name.'" value="'.htmlspecialchars($value).'"'.($readOnly ? ' readonly' : '').(is_null($maxLength) ? '' : ' maxlength="'.$maxLength.'"') . (is_null($customProperty) ? null : ' ' . $customProperty) . ' />';
  }
  
  static function hidden($name, $value = null, $cssClass = null, $customProperty = null, $noId = false) {
    return '<input type="hidden" class="'.$cssClass.'" name="'.$name.'" ' . ($noId ? '' : 'id="' . $name . '" ') . 'value="'.htmlspecialchars($value).'"' . (is_null($customProperty) ? null : ' ' . $customProperty) . ' />';
  }
  
  static function password($name, $value = null, $cssClass = null, $maxLength = null, $readOnly = false, $customProperty = null) {
    return '<input type="password" class="'.$cssClass.'" name="'.$name.'" id="'.$name.'" value="'.htmlspecialchars($value).'"'.($readOnly ? ' readonly' : '').(is_null($maxLength) ? '' : ' maxlength="'.$maxLength.'"') . (is_null($customProperty) ? null : ' ' . $customProperty) . ' />';
  }
  
  static function textarea($name, $value = null, $cssClass = null, $maxLength = null, $readOnly = false, $customProperty = null) {
    return '<textarea class="'.$cssClass.'" name="'.$name.'" id="'.$name.'" '.($readOnly ? ' readonly' : '') . (is_null($customProperty) ? null : ' ' . $customProperty) . '>' . htmlspecialchars($value) . '</textarea>';
  }
  
  static function imageSubmit($name, $value = null, $image = null, $alt = null, $cssClass = null, $customProperty = null, $jQButton = false) {
    return '<input type="image" src="'.$image.'" alt="'.$alt.'" class="' . (($jQButton) ? 'fg-button ui-state-default ui-corner-all ' : '') . $cssClass . '" name="'.$name.'" id="'.$name.'" value="'.htmlentities($value).'" '.$customProperty.' />';
  }

  static function submit($name, $value = null, $cssClass = null, $jQueryButton = false, $customProperty = null) {
    if ($jQueryButton) {
      funcUI::queueScript('js', 'bottom', 'ext', 'js/jquery/jquery.js');
      funcUI::queueScript('js', 'bottom', 'ext', 'js/jquery/jquery-ui.js');
    }
    return '<input type="submit" class="' . (($jQueryButton) ? 'fg-button ui-state-default ui-corner-all ' : '') . $cssClass . '" name="'.$name.'" id="'.$name.'" value="'.htmlentities($value).'"' . (is_null($customProperty) ? null : ' ' . $customProperty) . ' />';
  }

  static function button($name, $value = null, $cssClass = null, $jQueryButton = false, $customProperty = null) {
    if ($jQueryButton) {
      funcUI::queueScript('js', 'bottom', 'ext', 'js/jquery/jquery.js');
      funcUI::queueScript('js', 'bottom', 'ext', 'js/jquery/jquery-ui.js');
    }
    return '<input type="button" class="' . (($jQueryButton) ? 'fg-button ui-state-default ui-corner-all ' : '') . $cssClass . '" name="'.$name.'" id="'.$name.'" value="'.htmlentities($value).'"' . (is_null($customProperty) ? null : ' ' . $customProperty) . ' />';
  }
  
  static function validation($formName, $formCheck = 'formcheck') {
    funcUI::queueScript('css', 'top', 'ext', 'js/jquery/formcheck/theme/classic/formcheck.css', 1000);
    funcUI::queueScript('js', 'bottom', 'ext', 'js/jquery/formcheck/formcheck.js', 1001);
    funcUI::queueScript('js', 'bottom', 'embed', "jQuery(document).ready(function($){ $formCheck = new FormCheck('$formName'); });", 1002);
  }
}
?>