<?php
class funcXML {

  static function mergeSimplexml(&$simplexmlTo, &$simplexmlFrom) {
    foreach ($simplexmlFrom->attributes() as $attr_key => $attr_value) {
      $simplexmlTo->addAttribute($attr_key, $attr_value);
    }
    foreach ($simplexmlFrom->children() as $simplexmlChild) {
      $simplexml_temp = $simplexmlTo->addChild($simplexmlChild->getName(), (string) $simplexmlChild);
      funcXML::mergeSimplexml($simplexml_temp, $simplexmlChild);
    }
  }

}
?>