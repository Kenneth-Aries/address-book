<?php
class CrosstabReports {
  //variables use internally
  var $_ColumnNames = array();
  var $_WhereArray = array();
  var $_HavingArray = array();
  var $_groupbyFields = array();

  //variables populated when GenerateCrosstab is called
  var $sqlColumns = "";
  var $sqlCrosstab = "";
  var $arrayCrosstab = "";
  var $HTMLCrosstab = "";
  var $Error = false;

  //variables populated by the user before calling GenerateCrosstab
  var $fieldRow = "";
  var $fieldRowName = "";
  var $fieldColumn = "";
  var $fieldValue = "";
  var $table = "";
  var $orderby = "";
  var $where = array();
  var $groupbyfunction = "COUNT";
  var $groupbyfunctionHeader = "TOTAL";
  var $groupby = array();
  
  //variables for the HTML table
  var $tableId = ""; //id for the table for scripting purposes
  var $tableClass = ""; //css class for <table>
  var $tableProperties = ""; //properties for <table>
  var $tableRowHeaderClass = ""; //css class for <td> in 2nd <tr> to end in GROUPBY field columns (i.e. not the calculated values)
  var $tableColumnHeaderClass = ""; //css class for <td> in first <tr>
  var $tableValueClass = ""; //css class for all other <td>
 
  function _SplitWhereIntoHavingAndWhere($GROUPBY, $WHERE) {
    //debugging...
    //echo "<hr>".$GROUPBY;
    //echo "<br><br>";
    //print_r($WHERE);
    //echo "<br><br>";
    $ArrayWhere = array();
    $ArrayHaving = array();
    $ArrayGroupBy = array_map("trim", $this->_FieldsForSelect($GROUPBY));
    while (list(, $whereelement) = each($WHERE)) {
      //echo "|".$whereelement[0]."|<br>";
      if (in_array($whereelement[0], $ArrayGroupBy)) {
        //where field is same as group by field - move to HAVING clause
        //echo "Found: ".$whereelement[0]." (HAVING)<br>";
        array_push($ArrayHaving, implode(" ", $whereelement));
      }
      else {
        //echo "Did not find: ".$whereelement[0]." (WHERE)<br>";
        //add to WHERE - does not appear in GROUP BY field
        array_push($ArrayWhere, implode(" ", $whereelement));
      }
    }
    return array($ArrayWhere, $ArrayHaving);
  }
  
  function _FieldsForSelect($GROUPBY) {
    $itemnamelist = array();
    while (list(, $item) = each ($GROUPBY)) {
      array_push($itemnamelist, $item[0] . " AS \"" . $item[1]."\"");
    }
    return $itemnamelist;
  }
  
  function _FieldsForGroupBy($GROUPBY) {
    $itemlist = array();
    while (list(, $item) = each ($GROUPBY)) {
      if (strtolower(substr($item[0], 0, 5))!="count" && strtolower(substr($item[0], 0, 5))!="round" && strtolower(substr($item[0], 0, 3))!="sum" && strtolower(substr($item[0], 0, 3))!="avg") {
        array_push($itemlist, $item[0]);
      }
    }
    return $itemlist;
  }
  
  function _FieldsForTableHeader($GROUPBY) {
    $itemlist = array();
    while (list(, $item) = each ($GROUPBY)) {
      array_push($itemlist, $item[1]);
    }
    return $itemlist;
  }
  
  function _CheckForMySQLErrors() {
    $result = false;
    if (mysql_errno()) {
      $this->Error = "MySQL error ".mysql_errno().": ".mysql_error()."\n<br>When executing:<br>\n$this->sqlCrosstab\n<br>"; 
      $result = true;
    } 
    return $result;
  }

  function _GenerateSQL() {
    //get column names...
    $WhereAndHaving = $this->_SplitWhereIntoHavingAndWhere(array($this->fieldColumn), $this->where);
    
    $this->_WhereArray = $WhereAndHaving[0];
    $this->_HavingArray = $WhereAndHaving[1];
  
    $this->sqlColumns = "SELECT " . $this->fieldColumn . " FROM " . $this->table;
    if (count($this->_WhereArray) != 0) {
      $this->sqlColumns .= " WHERE " . implode(" AND ", $this->_WhereArray);
    }
    $this->sqlColumns .= " GROUP BY " . $this->fieldColumn;
    if (count($this->_HavingArray) != 0) {
      $this->sqlColumns .= " HAVING " . implode(" AND ", $this->_HavingArray);
    }
    if (strpos($this->orderby, $this->fieldColumn)) {
      $direction = substr($this->orderby, strpos($this->orderby, $this->fieldColumn) + strlen($this->fieldColumn) + 1, 3);
      if ($direction=="DES") {
        $direction = "DESC";
      }
      if ($direction == "ASC" || $direction == "DESC") {
        $this->sqlColumns .= " ORDER BY ".$this->fieldColumn." ".$direction;
      }
    }
  	$this->_ColumnNames = $GLOBALS['dbobject']->ExecuteQueryNamesOnly($this->sqlColumns);
  	if ($this->_CheckForMySQLErrors()) {
  	   return false;
  	}

    //echo $this->sqlColumns;
    //echo "<pre>";
    //print_r($this->_ColumnNames);
    //echo "</pre>";

    if (isset($this->_ColumnNames) && $this->_ColumnNames) {
      //loop through the column names and build sql
      if (count($this->groupby) != 0) {
        $this->_groupbyFields = $this->groupby;
      }
      array_push($this->_groupbyFields, array($this->fieldRow, $this->fieldRowName));
      //echo "<pre>";
      //print_r($this->_groupbyFields);
      //print_r($this->_FieldsForSelect($this->_groupbyFields));
      //echo "</pre>";
      $this->sqlCrosstab = "SELECT " . implode(", ", $this->_FieldsForSelect($this->_groupbyFields)) . $this->IIf($this->groupbyfunction, '', '', $this->IIf($this->groupbyfunction, 'CUSTOM', '', ", " . $this->groupbyfunction . "(" . $this->fieldValue . ") AS \"" . $this->groupbyfunctionHeader . "\""));
     	while (list(, $rowName) = each($this->_ColumnNames)) {
     	  if ($this->groupbyfunction == "CUSTOM") {
       	  $this->sqlCrosstab .= ", " . str_replace('<COLUMNHEADER>', $rowName[$this->fieldColumn], $this->fieldValue) . " AS `" . $rowName[$this->fieldColumn] . "`";
   	    }
     	  elseif ($this->groupbyfunction == "COUNT") {
       	  $this->sqlCrosstab .= ", SUM(IF(`" . $this->fieldColumn . "` = '" . $rowName[$this->fieldColumn] . "', 1, 0)) AS `" . $rowName[$this->fieldColumn] . "`";
       	}
       	else {
       	  $this->sqlCrosstab .= ", " . $this->groupbyfunction . "(" . $this->fieldValue . ") AS `" . $rowName[$this->fieldColumn] . "`";
       	}
      }
      $this->sqlCrosstab .= " FROM " . $this->table;
     
      $WhereAndHaving = $this->_SplitWhereIntoHavingAndWhere($this->_groupbyFields, $this->where);
    
      $this->_WhereArray = $WhereAndHaving[0];
      $this->_HavingArray = $WhereAndHaving[1];

      if (count($this->_WhereArray) != 0) {
        $this->sqlCrosstab .= " WHERE " . implode(" AND ", $this->_WhereArray);
      }
        $this->sqlCrosstab .= " GROUP BY " . implode(", ", $this->_FieldsForGroupBy($this->_groupbyFields));
      if (count($this->_HavingArray) != 0) {
        $this->sqlCrosstab .= " HAVING " . implode(" AND ", $this->_HavingArray);
      }
      if ($this->orderby != "") {
        $this->sqlCrosstab .= " ORDER BY " . $this->orderby;
      }
      $this->sqlCrosstab .= ";";
      //debugging
      //exit($sql_crosstab);
      return true;
    }
    $this->Error = "Error generating Columns: ".$this->sqlColumns;
    return false;
  }
  
  function _getArray() {
  	$this->arrayCrosstab = $GLOBALS['dbobject']->ExecuteQueryNamesOnly($this->sqlCrosstab);
  	if ($this->_CheckForMySQLErrors()) {
  	   return false;
  	}
  	return true;
  }
  
  function IIf($var, $value, $true, $false) {
    if ($var == $value) {
      return $true;
    }
    else {
      return $false;
    }
  }
  
/*
  var $tableRowHeaderClass = "";
  var $tableColumnHeaderClass = "";
  var $tableValueClass = "";
*/

  function _getHTMLTable($header = true) {
    $HTML = "";
    if (isset($this->arrayCrosstab) && $this->arrayCrosstab) {
      $HTML = "<table " . $this->IIf($this->tableId, "", "", " id=\"".$this->tableId."\"") . $this->IIf($this->tableProperties, "", "", " ".$this->tableProperties) . $this->IIf($this->tableClass, "", "", " class=\"".$this->tableClass."\"") . ">";
      if ($header) {
        $HTML .= "<tr>";
        if (count($this->groupby) != 0) {
          $HTML .= "<td " . $this->IIf($this->tableColumnHeaderClass, "", "", " class=\"".$this->tableColumnHeaderClass."\"") . " valign=\"bottom\">";
          $HTML .= implode("</td><td " . $this->IIf($this->tableColumnHeaderClass, "", "", " class=\"".$this->tableColumnHeaderClass."\"") . " valign=\"bottom\">", array_map("trim", $this->_FieldsForTableHeader($this->groupby)));
          $HTML .= "</td>";
        }
        $HTML .= "<td " . $this->IIf($this->tableColumnHeaderClass, "", "", " class=\"".$this->tableColumnHeaderClass."\"") . " valign=\"bottom\"><img src =\"script.reportnames.php?Text=".str_replace("\"", "", $this->fieldRowName)."\" height=120 width=25></td>".$this->IIf($this->groupbyfunction, '', '', $this->IIf($this->groupbyfunction, 'CUSTOM', '', "<td " . $this->IIf($this->tableColumnHeaderClass, "", "", " class=\"".$this->tableColumnHeaderClass."\"") . ">" . $this->groupbyfunctionHeader . "</td>"));
        foreach ($this->_ColumnNames as &$ColumnName) {
          $HTML .= "<td " . $this->IIf($this->tableColumnHeaderClass, "", "", " class=\"".$this->tableColumnHeaderClass."\"") . " valign=\"bottom\"><img src =\"script.reportnames.php?Text=".str_replace("\"", "", $ColumnName[$this->fieldColumn])."\" height=120 width=25></td>";
        }
        $HTML .= "</tr>";
      }
      foreach ($this->arrayCrosstab as &$row) {
        $HTML .= "<tr>";
        $countCols = 1;
        foreach ($row as &$value) {
          //count($this->groupby) GROUP BY COLUMNS
          //1 OVERALL column
          //count($this->_ColumnNames) COLUMNS FROM PIVOT
          if ($this->groupbyfunction == 'CUSTOM') {
            $offset = 1;
          }
          else {
            $offset = 2;
          }
          if ($countCols <= count($this->groupby) + $offset) {
            if ($countCols == count($this->groupby) + $offset) {
              //final one before the data...
              $HTML .= "<td " . $this->IIf($this->tableRowHeaderClass, "", "", " class=\"containerLabel_report\"") . " align=right>".str_replace(' to ', ' to<br>', $value)."</td>";
            }
            else {
              $HTML .= "<td " . $this->IIf($this->tableRowHeaderClass, "", "", " class=\"".$this->tableRowHeaderClass."\"") . " align=right>".str_replace(' to ', ' to<br>', $value)."</td>";
            }
          }
          else {
            $HTML .= "<td " . $this->IIf($this->tableValueClass, "", "", " class=\"".$this->tableValueClass."\"") . " align=right>".str_replace(' to ', ' to<br>', $value)."</td>";
          }
          $countCols ++;
        }
        $HTML .= "</tr>";
      }
      $HTML .= "</table>";
    }
    $this->HTMLCrosstab = $HTML;
    return true;
  }
  
  function GenerateCrosstab() {
    $result = false;
    if ($this->_GenerateSQL()) {
      if ($this->_getArray()) {
        if ($this->_getHTMLTable()) {
          $result = true;
          return $result;
        }
      }
    }
    return $result;
  }
}
?>