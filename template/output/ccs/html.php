<?php
if (!isset($title) || empty($title)) {$title = '';} else {$title = ' - '.$title;}
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<title><?php echo $GLOBALS['app.title'].$title; ?></title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['app.ui.theme.folder']; ?>css/jquery-ui.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['app.ui.theme.folder']; ?>css/style.css" />
  <?php echo $cssTop . $styleTop . $jsTop . $scriptTop;?>
</head>

<body>
<table id="tblContent" width="100%" border="0" cellpadding="0" cellspacing="0"<?php echo ((funcArray::get($_SESSION, 'uiCompact', 'N') == 'Y') ? ' class="compact"' : '') ?>>
  <tr>
    <td valign="top" style="padding-left:40px; padding-right:40px;"><table width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td height="116"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><p class="customer_text"><?php echo $GLOBALS['app.title']; ?></p></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td height="36"><table width="100%" border="0" cellspacing="0" cellpadding="0" id="menu" class="white_text">
            <tr>
              <td><?php echo $menu; ?></td>
              <td></td>
              <td width="300" align="right">
<?php
if (funcCore::requireClasses($GLOBALS['app.var.user.classname'])) {
  echo funcUI::getPage('list.php', 'contacts');
}
?>        
              </td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td class="content" style="padding-top:25px; padding-bottom:25px;">
          <div class="ui-widget">
<?php echo $content; ?>
          </div>
        </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="50" style="background-image:url(<?php echo $GLOBALS['app.ui.theme.folder']; ?>images/footer_url.gif);padding:0 40px">
        <span style="float:right">
          <a href="http://www.bottomline.co.za/" target="_blank" class="bli" title="bottomline interactive"></a>
          <a href="http://www.ccs.co.za/" target="_blank" class="ccs" title="customer care solutions"></a>
          <a href="http://www.simdirect.co.za/" target="_blank" class="sim" title="sim direct"></a>
        </span>
    </td>
  </tr>
</table>
<div id="dialog" style="display:none" title="<?php echo htmlspecialchars($GLOBALS['app.title']); ?>"></div>
<div id="indicator" style="display:none"><img src="<?php echo $GLOBALS['app.ui.theme.folder']; ?>images/indicator.gif" /></div>
<?php echo $cssBottom . $styleBottom . $jsBottom . $scriptBottom; ?>
</body>
</html>