<?php
if (!isset($title) || empty($title)) {$title = '';} else {$title = ' - '.$title;}
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<title><?php echo $GLOBALS['app.title'] . $title; ?></title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <link rel="stylesheet" href="<?php echo $GLOBALS['app.ui.theme.folder']; ?>css/style.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo $GLOBALS['app.ui.theme.folder']; ?>css/jquery-ui-1.8.14.custom.css" type="text/css" />
  <?php echo $cssTop . $styleTop . $jsTop . $scriptTop;?>
</head>

<body>
<div id="sitecontainer">
  <div id="header">
    <div style="float:right"><h1><?php echo $GLOBALS['app.title']; ?></h1></div>
    <div style="float:left"><img src="<?php echo $GLOBALS['app.ui.theme.folder']; ?>images/logo.jpg" width="81" height="40" alt="Logo" /></div>
  </div>
  <div style="clear:both"></div>
  <hr />
  <!-- MAIN CONTENT SECTION -->
  <?php echo $content; ?>
  <!-- END MAIN CONTENT SECTION -->
  <div style="clear:both"></div>
  <hr />
  <div id="footer">
    <div style="float:right"><a onclick="return false;" href="#">Terms &amp; Conditions</a></div>
    <div style="float:left">&copy; <?php echo $GLOBALS['app.title'] . ' ' . Date('Y'); ?>. All Rights Reserved.</div>
  </div>
</div>
<?php echo $cssBottom . $styleBottom . $jsBottom . $scriptBottom; ?>
</body>
</html>