<?php
  funcUI::queueScript('js', 'bottom', 'ext', 'js/jquery/jquery.js');
  funcUI::queueScript('js', 'bottom', 'ext', 'js/jquery/jquery-ui.js');
  funcUI::queueScript('js', 'bottom', 'ext', 'js/ui.script.js');

  $content .= '<div id="panel_login">';
  if (isset($GLOBALS['app.user.loggedin']) && $GLOBALS['app.user.loggedin']) {
    $content .= 'Welcome '.$GLOBALS['app.user']->$GLOBALS['app.var.user.name'].' '.funcForm::form('frmLogout', 'post', 'logout.php').funcForm::submit('btnLogout', 'Logout', null, true).funcForm::closeForm();
  }
  else {
    funcCore::requireClasses($GLOBALS['app.var.user.classname']);
    
    funcForm::validation('frmLogin');
    $content .= '
' . funcForm::form('frmLogin', 'post') . funcForm::hidden('request', 'login') . '
<table border=0>
<tr><td>Email</td>
    <td>'.funcForm::text($GLOBALS['app.var.user.email'], null, 'input validate[\'required\']', 50).'</td></tr>
<tr><td>Password</td>
    <td>'.funcForm::password($GLOBALS['app.var.user.password'], null, 'input validate[\'required\']', 50).'</td></tr>
<tr><td colspan="2" align="right">'.funcForm::submit('btnLogin', 'Login', null, true).'</td></tr>
</table>
</form>
';
  }
  $content .= '</div>';
?>