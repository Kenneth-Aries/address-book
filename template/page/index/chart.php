<?php
require_once $GLOBALS['app.folder.include'] . 'class.func.imaginggooglechart.php';

$chart = new funcImagingGoogleChart('Skeleton Cove Pirate Report', '3D Pie Chart', 700, 400, 100, 10);

$chart->addLegendText('Gold coins');
$chart->addAxisLabel(77, 'x');
$chart->addDatum(77);

$chart->addLegendText('Unopened treasure chests');
$chart->addAxisLabel(6, 'x');
$chart->addDatum(6);

$chart->addLegendText('Bottles of rum');
$chart->addAxisLabel(34, 'x');
$chart->addDatum(34);

$chart->addLegendText('Parrots');
$chart->addAxisLabel(1, 'x');
$chart->addDatum(1);

$imgSrc = $chart->getUri();

$content .= '<img src="' . $imgSrc . '" ?>';
?>