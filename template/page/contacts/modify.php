<?php
/**
 * Created by PhpStorm.
 * User: kennethp
 * Date: 11/03/2019
 * Time: 06:47 PM
 */
funcCore::requireClasses('contacts');
$contactId = funcArray::get($_REQUEST, 'contactId');
$searchText  = funcArray::get($_REQUEST, 'txtSearchText');
$contact = new Contacts($contactId);
$count = 1;


$js = <<<JS
jQuery(document).ready(function($) {
  var contactNumberCounter = 1;
  var emailAddressCounter = 1;
  $('#addContactNumber').on("click", function(){
    ++contactNumberCounter;
    var newContactNumberDiv = $('#ContactNumbers1').clone();
    
    newContactNumberDiv.find('input,div').each(function() {
        $(this).val('');
        this.name= this.name.replace('1', contactNumberCounter);
        $(this).attr('id', this.name.replace('1', contactNumberCounter));
    });

    newContactNumberDiv.appendTo('#ContactNumbers').attr("id", "ContactNumbers" + contactNumberCounter);    

  });
  
  $('#addEmailAddress').on("click", function(){
    // alert('here');
    // return false;
    ++emailAddressCounter;
    var newEmailDiv = $('#EmailAddress1').clone();
    
    newEmailDiv.find('input,div').each(function() {
        $(this).val('');
        this.name= this.name.replace('1', emailAddressCounter);
        $(this).attr('id', this.name.replace('1', emailAddressCounter));
    });

    newEmailDiv.appendTo('#EmailAddresses').attr("id", "EmailAddress" + emailAddressCounter);    

  });

  
  $('#btnBack').click(function(){
    window.location = 'home.php?module=contacts&action=list&txtSearchText=$searchText';
  });
  
   })
JS;
funcUI::queueScript('js', 'bottom', 'embed', $js);
funcUI::queueScript('css', 'top', 'ext', $GLOBALS['app.ui.theme.folder'] . 'css/tablesorter/blue/style.css');

if (!empty($contact)) {
  $content .=  funcForm::validation('frmModifyContacts');
  $content .= funcForm::form('frmModifyContacts') . funcForm::hidden('contactId', $contactId) . funcForm::hidden('txtSearchText', $searchText)  .'
  
  <span style="color:#7ca1dd;font-weight:bold;"><div id="Suggestion" colspan="3"></div></span>
  <table border="0" style="border-collapse:collapse;" width="400" cellpadding="3" >     
    <tbody>';
  $content .= '
    <span style="margin-bottom: 15px;">      
    <tr>
        <td><b>Contact Id : <b></td><td>' . $contact->Id . '</td>
      </tr> 
      <tr>
        <td>Firstname</td><td>' . funcForm::text('txtFirstName', $contact->FirstName, "validate['required'] form-control") . '</td>
      </tr>  
       <tr>
        <td>Surname</td><td>' . funcForm::text('txtSurname', $contact->Surname, "validate['required'] form-control") . '</td>
      </tr>';
      if (empty($contact->Id)) {
        $content .= '</tbody></table>';

        $content .= '<div style="margin-top: 15px;"><a href="#" id="addContactNumber" class="btn btn-default">Add Contact Number</a></div>
        <div id="ContactNumbers" style="width: 35%">
          <div id="ContactNumbers1"><label>Contact Number</label><span style="margin-left: 10px;">' . funcForm::text('txtContactNumber[' . $count . ']', null, 'validate[\'%requiredIfVisible\'] form-control') . '</span></div>
         </div>';

        $content .= '<div style="margin-top: 15px;"><a href="#" id="addEmailAddress" class="btn btn-default">Add Email Address</a></div>
        <div id="EmailAddresses" style="width: 35%">
          <div id="EmailAddress1"><label>Email</label><span style="margin-left: 10px;">' . funcForm::text('txtEmail[' . $count . ']', null, 'validate[\'%requiredIfVisible\'] form-control') . '</span></div>
        </div>';
      }
      else {
        $contactNumbers = json_decode($contact->ContactNumbers, true);
        $emailAddresses = json_decode($contact->EmailAddresses, true);
        if (!empty($contactNumbers)) {
          $count = 1;
          foreach ($contactNumbers as $contactNumber) {
            $content .= '<tr><td>Contact Number ' . $count . '</td><td>' . funcForm::text('txtContactNumber[' . $count . ']', $contactNumber, "form-control") . '</td></tr>';
            $count++;
          }
        }
        $content .= '</td><td>';
        if (!empty($emailAddresses)) {
          $count = 1;
          foreach ($emailAddresses as $emailAddress) {
            $content .= '<tr><td>Email ' . $count . '</td><td>' . funcForm::text('txtEmail[' . $count . ']', $emailAddress, "form-control") . '</td></tr>';
            $count++;
          }
        }
        $content .= '</tbody></table>';
      }
  $content .= '
<div style="margin: 15px; 90px; 20px; 0px;">' . funcForm::submit('btnSave', 'Save', 'form-control', false, 'style="width:20%; float:left"') . funcForm::button('btnBack', 'Back', 'form-control', false, 'style="width:20%; float:left; margin-left:20px"').'</div><br /><br />
</div>';
} else {
  $content .= '<div style="margin-top: 15px;">No contact found</div>';
}
$content .= funcForm::closeForm();

?>