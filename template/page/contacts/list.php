<?php
/**
 * Created by PhpStorm.
 * User: kennethp
 * Date: 11/03/2019
 * Time: 05:51 PM
 */
funcCore::requireClasses('contacts');
require_once $GLOBALS['app.folder.include'] . 'class.pagination.php';

$searchText = funcArray::get($_REQUEST, 'txtSearchText');

$js = <<<JS
jQuery(document).ready(function($) {    
  $('table.tablesorter').tablesorter({});

  $('#removeContact').click(function() {
    if(!confirm('Are you sure you want to remove this contact?')){
      return false;
    }
  })
});
JS;
funcUI::queueScript('js', 'bottom', 'embed', $js);
funcUI::queueScript('js', 'bottom', 'ext', 'js/jquery/jquery.tablesorter.min.js');
funcUI::queueScript('css', 'top', 'ext', $GLOBALS['app.ui.theme.folder'] . 'css/tablesorter/blue/style.css');
funcForm::validation('frmContacts');
$content .= funcForm::form('frmContacts') .
$content .= funcForm::hidden('action', $action);
$content .= funcForm::hidden('module', $module).
  '
<div><a href="home.php?module=contacts&action=modify" class="btn btn-default">Add contacts</a></div><br />
<table style="float:left" celllpadding="2" class="margin15">
  <tr><td>Search By Name</td><td>' . funcForm::text('txtSearchText', $searchText, "form-control", null, false, 'style="width: 200px;"') . '</td>
  <td>' . funcForm::submit('btnSearch', 'Search', 'form-control', false, 'style="width:100%"') . '</td>
  </tr>
  <tr>
    <td height="20"></td>
  </tr>  
</table>

<div style="margin-top: 1px;float:left"></div><br /><br />';

$where = array();

if(!empty($searchText)) {
  $where[] = '( `FirstName` Like "%' . $searchText . '%" OR `Surname` Like "%' . $searchText . '%" OR `ContactNumbers` Like "%' . $searchText . '%" OR `EmailAddresses` Like "%' . $searchText . '%" )';
}

$contactsCount = Contacts::count(implode(" AND ",$where));
$page = new Pagination('p', 'rpp', $contactsCount);
$page->rpp = 10;
$contacts = Contacts::get(null,implode(" AND ",$where),null,null,$page->page, $page->rpp);

$content .= '<div style="padding: 10px;border:0px solid #000;clear:both;">' . $page->generateLinks('?module=' . $module . '&action=' . $action  . (!empty($searchText) ? '&txtSearchText=' . $searchText : null)). '</div>';
if (!empty($contacts)) {

  $content .= '<table style="border-collapse:collapse" border="0" cellpadding="3" width="60%"><thead>';
  $content .= '<tr><td width="20%" colspan="2"><b>Contacts : '.$contactsCount.'</b></td></tr>';
  $content .= '</table>';
  $content .= '<table style="border-collapse:collapse" border="1" cellpadding="3" width="100%" class="tablesorter" ><thead>';
  $content .= '<tr><th>FirstName</th><th>Surname</th><th>Contact Numbers</th><th>Contact Email Addresses</th><th></th></tr></thead><tbody id="operatorTableRows">';
  foreach($contacts as $contact) {
    $content .= '<tr>
                     <td>' . $contact->FirstName . '</td>
                     <td>' .  $contact->Surname  . '</td>
                     <td>';
    $contactNumbers = json_decode($contact->ContactNumbers, true);
    $emailAddresses = json_decode($contact->EmailAddresses, true);
    if (!empty($contactNumbers)) {
      $count = 1;
      foreach ($contactNumbers as $contactNumber) {
        $content .= '<div>Contact ' . $count . ': ' . $contactNumber . '</div>';
        $count++;
      }
    }
    $content .= '</td><td>';
    if (!empty($emailAddresses)) {
      $count = 1;
      foreach ($emailAddresses as $emailAddress) {
        $content .= '<div>Email ' . $count . ': ' . $emailAddress . '</div>';
        $count++;
      }
    }

    $content .= '</td><td>  
                       <a href="home.php?module=contacts&action=modify&contactId=' . $contact->Id . '&txtSearchText=' . $searchText . '" class="btn btn-warning">Edit</a><br />
                       <a id="removeContact" href="home.php?method=script&module=contacts&action=delete&contactId=' . $contact->Id . '&txtSearchText=' . $searchText . '" class="btn btn-danger">Delete</a> 
                     </td>

                 </tr>';
  }
  $content .= '</tbody></table>';
} else {
  $content .= 'No contacts found';
}
$content .= '<div style="padding: 10px;border:0px solid #000;clear:both;">' . $page->generateLinks('?module=' . $module . '&action=' . $action  . (!empty($searchText) ? '&txtSearchText=' . $searchText : '') . (!empty($searchUserStatus) ? '&ddSearchUserStatus=' . $searchUserStatus : '') . (!empty($searchUserType) ? '&ddSearchUserType=' . $searchUserType : '')). '</div>';
$content .= funcForm::closeForm();
?>