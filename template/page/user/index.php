<?php
funcCore::requireClasses('user');
echo 'HEre';
exit;

require_once($GLOBALS['app.folder.include'] . 'class.pagination.php');

$order = 'Id ASC';
$passedOrder = funcArray::get($_GET, 'order');
if(isset($passedOrder) && $passedOrder != null) {
  $order = $passedOrder;
}

$user = User::get(null, null, $order);
$countUser = User::count();
                                                        
$content .= '
    <table border="0">
        <tr>
            <td><a class="fg-button ui-state-default ui-corner-all" href="home.php?module=user&action=modify">Add user</a></td>
        </tr>
    </table>
    <hr />
    <h1>' . $countUser . ' users</h1>
    <table id="maintable" border="1" cellspacing="0" cellpadding="2px">
        <tr>
            <th>Action</th>
            <th><a href="home.php?module=user&action=index&order=' . ($order == 'Username ASC' ? urlencode('Username DESC') : urlencode('Username ASC')) . '">Username ' . (strpos($order, 'Username') !== FALSE ? strpos($order, 'ASC') !== FALSE ? '&darr;' : '&uarr;' : '') . '</a></th>
            <th><a href="home.php?module=user&action=index&order=' . ($order == 'Email ASC' ? urlencode('Email DESC') : urlencode('Email ASC')) . '">Email ' . (strpos($order, 'Email') !== FALSE ? strpos($order, 'ASC') !== FALSE ? '&darr;' : '&uarr;' : '') . '</a></th>
            <th><a href="home.php?module=user&action=index&order=' . ($order == 'FullName ASC' ? urlencode('FullName DESC') : urlencode('FullName ASC')) . '">Full Name ' . (strpos($order, 'FullName') !== FALSE ? strpos($order, 'ASC') !== FALSE ? '&darr;' : '&uarr;' : '') . '</a></th>
            <th>Status</th>
            <th>Type</th>
        </tr>
';

foreach ($user as $u) {
    $content .= '
        <tr>
            <td><a href="home.php?module=user&action=modify&id=' . $u->Id . '">modify</a></td>
            <td>' . $u->Username . '</td>
            <td>' . $u->Email . '</td>
            <td>' . $u->FullName . '</td>
            <td>' . $u->Status . '</td>
            <td>' . $u->Type . '</td>
        </tr>
    ';
}

$content .= '
    </table>
';

?>