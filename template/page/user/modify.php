<?php
funcCore::requireClasses('user');

$userId = funcArray::get($_GET, 'id');
$user = new User;
$lookupResult = $user->lookup($userId);

$statusOptions = array();
$statusOptions[] = funcForm::prepOption('ACTIVE', 'Active');
$statusOptions[] = funcForm::prepOption('INACTIVE', 'Inactive');

$typeOptions = array();
$typeOptions[] = funcForm::prepOption('NORMAL', 'User');
$typeOptions[] = funcForm::prepOption('ADMIN', 'Admin');

$js = <<<JS
jQuery(document).ready(function($) {
  $('#btnBack').click(function(e) {
    window.location = 'home.php?module=user&action=index';
  });
  $('#btnDelete').click(function(e) {
    window.location = 'home.php?module=user&action=delete&method=script&id={$userId}';
  });
});
JS;
funcUI::queueScript('js', 'bottom', 'embed', $js);

funcForm::validation('frmUser');
$content .=  funcForm::form('frmUser') .
              funcForm::hidden('module', 'user') .
              funcForm::hidden('action', 'save') .
'   <table border="0">
            <tr><th>ID</th>
                <td>' . $user->Id . '</td></tr>
            <tr><th>Username</th>
                <td>' . funcForm::text('Username', $user->Username, "input validate['required']") . '</td></tr>
            <tr><th>Password</th>
                <td>' . funcForm::password('Password', null, "input") . '</td></tr>
          <tr><th>Confirm Password</th>
              <td>' . funcForm::password('Password2', null, "input validate['confirm[Password]']") . '</td></tr>
            <tr><th>Email</th>
                <td>' . funcForm::text('Email', $user->Email, "input validate['required']") . '</td></tr>
          <tr><th>Full Name</th>
                <td>' . funcForm::text('FullName', $user->FullName, "input validate['required']") . '</td></tr>
            <tr><th>Status</th>
                <td>' . funcForm::select('Status', $user->Status, null, $statusOptions, "select") . '</td></tr>
            <tr><th>Type</th>
                <td>' . funcForm::select('Type', $user->Type, null, $typeOptions, "select") . '</td></tr>
';
            
$content .= '
        <tr><td align="right" colspan="2">
        ' . (!empty($user->Id) ? funcForm::button('btnDelete', 'Delete', null, true) : null) .
          '&nbsp&nbsp' .
          funcForm::button('btnBack', 'Back', null, true) .
          '&nbsp&nbsp' .
          funcForm::submit('btnSubmit', 'Save', null, true) .
        '</td></tr>
    </table>
    ' . funcForm::closeForm()
;
?>