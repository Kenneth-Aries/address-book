<?php
funcCore::requireClasses('user');
$user = new User;

$userId = funcArray::get($_REQUEST, 'id');
$user->lookup($userId);

$user->Username = funcArray::get($_POST, 'Username');

$password = funcArray::get($_POST, 'Password');
if (!empty($password)) {
  $user->Password = User::encryptPassword($password);
}

$user->Email= funcArray::get($_POST, 'Email');
$user->FullName = funcArray::get($_POST, 'FullName');
$user->Status = funcArray::get($_POST, 'Status');
$user->Type = funcArray::get($_POST, 'Type');

if ($user->save()) {
  if (!empty($password) && $user->Id == $GLOBALS['app.user']->Id) {
    User::updateCurrentUserCredentials($user->$GLOBALS['app.var.user.email'], $user->$GLOBALS['app.var.user.password']);
  }

  funcCore::redirect('home.php?module=user&action=modify&id=' . $user->Id, 'Saved successfully', $GLOBALS['app.alert.success']);
}
funcCore::redirect('home.php?module=user&action=modify', 'There was an error saving the user', $GLOBALS['app.alert.error']);
?>