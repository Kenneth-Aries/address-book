<?php
funcCore::requireClasses('user');

$userId = funcArray::get($_REQUEST, 'id');

$user = new User;
$user->lookup($userId);
$result = $user->delete();

if ($user->delete()) {
  funcCore::redirect('home.php?module=user&action=index', 'Deleted successfully', $GLOBALS['app.alert.success']);
}
else {
  funcCore::redirect('home.php?module=user&action=index', 'There was an error deleting the user', $GLOBALS['app.alert.error']);
}
?>