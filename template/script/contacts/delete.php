<?php
/**
 * Created by PhpStorm.
 * User: kennethp
 * Date: 11/03/2019
 * Time: 08:56 PM
 */
funcCore::requireClasses('contacts');

$contactId = funcArray::get($_GET, 'contactId');
if (!empty($contactId)) {
  $contact = new Contacts($contactId);
  if ($contact->delete()) {
    funcCore::redirect('home.php?module=contacts&action=list&txtSearchText=' . $criteria, 'Saved Successfully', $GLOBALS['app.alert.success']);
  }
}
funcCore::redirect('home.php?module=contacts&action=list&txtSearchText=' . $criteria, 'There was an error removinf this contact', $GLOBALS['app.alert.warning']);
