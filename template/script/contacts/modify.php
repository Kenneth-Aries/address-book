<?php
/**
 * Created by PhpStorm.
 * User: kennethp
 * Date: 11/03/2019
 * Time: 07:58 PM
 */
funcCore::requireClasses('contacts');

$contactId = funcArray::get($_POST, 'contactId');
$contactNumbers = funcArray::get($_POST, 'txtContactNumber');
$emailAddreses = funcArray::get($_POST, 'txtEmail');
$criteria = funcArray::get($_POST, 'txtSearchText');

$contact = new Contacts($contactId);
$contact->FirstName = funcArray::get($_POST, 'txtFirstName');
$contact->Surname = funcArray::get($_POST, 'txtSurname');
$contact->ContactNumbers = json_encode($contactNumbers);
$contact->EmailAddresses = json_encode($emailAddreses);

if ($contact->save()) {
  funcCore::redirect('home.php?module=contacts&action=list&txtSearchText=' . $criteria, 'Saved Successfully', $GLOBALS['app.alert.success']);
}
else {
  funcCore::redirect('home.php?module=contacts&action=modify', 'Saved Successfully', $GLOBALS['app.alert.warning']);
}


