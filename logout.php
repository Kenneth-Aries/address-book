<?php
require_once('include/setup.app.php');
session_destroy();
session_start();
funcCore::redirect(null, 'You have been successfully logged out', $GLOBALS['app.alert.success']);
?>