<?php
require_once('include/setup.app.php');

if (empty($action)) {
  $action = 'index';
}

$content .= funcUI::getPage($action . '.php', 'api');

funcUI::renderOutput($GLOBALS['app.ui.theme'], 'data.php');
?>