jQuery(function() {
  FormCheck = function(form , ajax ) {
    this.init(form,ajax)
  }

  jQuery.extend(FormCheck.prototype, {
    ajax : false,
    isValid : true,    
    alerts: {
      required: 'This field is required.',
      alpha: 'This field accepts alphabetic characters only.',
      alphanum: 'This field accepts alphanumeric characters only.',
      nodigit: 'No digits are accepted.',
      digit: 'Please enter a valid integer.',
      digitltd: 'The value must be between %0 and %1',
      number: 'Please enter a valid number.',
      email: 'Please enter a valid email.',
      image: 'This field should only contain image types',
      phone: 'Please enter a valid phone.',
      phone_inter: 'Please enter a valid international phone number.',
      url: 'Please enter a valid url.',
      confirm: 'This field is different from %0',
      differs: 'This value must be different of %0',
      length_str: 'The length is incorrect, it must be between %0 and %1',
      length_fix: 'The length is incorrect, it must be exactly %0 characters',
      lengthmax: 'The length is incorrect, it must be at max %0',
      lengthmin: 'The length is incorrect, it must be at least %0',
      words_min: 'This field must concain at least %0 words, currently: %1 words',
      words_range: 'This field must contain %0-%1 words, currently: %2 words',
      words_max: 'This field must contain at max %0 words, currently: %1 words',
      checkbox: 'Please check the box',
      radios: 'Please select a value',
      select: 'Please choose a value'
    },
    regexp: {
      required: /[^.*]/,
      alpha: /^[a-z ._-]+$/i,
      alphanum: /^[a-z0-9 ._-]+$/i,
      digit: /^[-+]?[0-9]+$/,
      nodigit: /^[^0-9]+$/,
      number: /^[-+]?\d*\.?\d+$/,
      email: /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/,
      image: /.(jpg|jpeg|png|gif|bmp)$/i,
      phone: /^[\d\s ().-]+$/, // alternate regex : /^[\d\s ().-]+$/,/^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$/
      phone_inter: /^\+{0,1}[0-9 \(\)\.\-]+$/,
      url: /^(http|https|ftp)\:\/\/[a-z0-9\-\.]+\.[a-z]{2,3}(:[a-z0-9]*)?\/?([a-z0-9\-\._\?\,\'\/\\\+&amp;%\$#\=~])*$/i
    },
    init: function(form,ajax) {
      this.ajax = ajax || false;
      var form = document.forms[form];
      var _self = this;
      this.form = form;

      if(this.ajax !== false){

        jQuery(form).find(":button").each(function(){

            jQuery(this).click(function(){
                var parent = jQuery(this).parent();
                var list = parent.find("*[class*=validate]");

                var listEnd = list.length;

                for (var p = 0; p < listEnd; p++) {
                  var element = list[p];
                  var property = element.getAttribute('class').split(' ');
                  _self.isValid = false;

                  for (var u = 0; u < property.length; u++) {
                    var classX = property[u];

                    if (classX.match(/^validate(\[.+\])$/)) {
                      var validators = eval(classX.match(
                        /^validate(\[.+\])$/)[1]);

                      _self.isValid = _self.validate(element, validators);
                    }
                  }

                  if(!_self.isValid){
                    break;
                  }
                }
            });
        });
      }

      jQuery(form).submit(function(event) {

        var classList = form.querySelectorAll("*[class*=validate]");
        var loopEnd = classList.length;

        for (var i = 0; i < loopEnd; i++) {
          var element = classList[i];
          var property = element.getAttribute('class').split(' ');
          _self.isValid = false;

          for (var j = 0; j < property.length; j++) {
            var classX = property[j];

            if (classX.match(/^validate(\[.+\])$/)) {
              var validators = eval(classX.match(
                /^validate(\[.+\])$/)[1]);

              _self.isValid = _self.validate(element, validators);
            }
          }

          if(!_self.isValid){
            break;
          }
        }

        if( !_self.isValid ){
          event.preventDefault();
        }
      });
    },
    validate: function(el, validators) {

      el.errors = [];

      var target = null;
      var index = this.getIndex(validators);

      if(index != null){
        target = validators.splice(index,1).toString().split(':')[1];
      }

      for (var n = 0; n < validators.length; n++) {

        var valtype = (validators[n].indexOf(
          '[') !== -1 ? validators[n].substring(0, validators[n].indexOf(
          '[')) : validators[n]);

        var elValue = jQuery(el).val();
        var isValid = true;

        if(validators[n].match(/^%.+/)){
          isValid = window[eval([validators[n].substring(validators[n].indexOf('%') + 1)])](el);
        }

        switch (valtype) {
          case 'required':
            if (elValue == "" || !elValue.match(this.regexp.required)) {
              el.errors.push(this.alerts.required);
              isValid = false;
            }else if(el.type == 'checkbox'){
                isValid = this.simpleValidate(el);
                el.errors.push(this.msg);
            }else{
              if( el.type == 'radio' ){
                isValid = this.validateGroup(el);
              }
            }
            break;
          case 'alpha':
            if (elValue == "" || !elValue.match(this.regexp.alpha)) {
              el.errors.push(this.alerts.alpha);
              isValid = false;
            }
            break;
          case 'alphanum':
            if (elValue == "" || !elValue.match(this.regexp.alphanum)) {
              el.errors.push(this.alerts.alphanum);
              isValid = false;
            }
            break;
          case 'digit':
            if (elValue == "" || !elValue.match(this.regexp.digit)) {
              el.errors.push(this.alerts.digit);
              isValid = false;
            }
            break;
          case 'nodigit':
            if (elValue == "" || !elValue.match(this.regexp.nodigit)) {
              el.errors.push(this.alerts.nodigit);
              isValid = false;
            }
            break;
          case 'number':
            if (elValue == "" || !elValue.match(this.regexp.number)) {
              el.errors.push(this.alerts.number);
              isValid = false;
            }
            break;
          case 'email':
            if (elValue == "" || !elValue.match(this.regexp.email)) {
              isValid = false;
              el.errors.push(this.alerts.email);
            }
            break;
          case 'image':
            if (elValue == "" || !elValue.match(this.regexp.image)) {
              el.errors.push(this.alerts.image);
              isValid = false;
            }
            break;
          case 'phone':
            if (elValue == "" || !elValue.match(this.regexp.phone)) {
              el.errors.push(this.alerts.phone);
              isValid = false;
            }
            break;
          case 'phone_inter':
            if (elValue == "" || !elValue.match(this.regexp.phone_inter)) {
              el.errors.push(this.alerts.phone_inter);
              isValid = false;
            }
            break;
          case 'url':
            if (elValue == "" || !elValue.match(this.regexp.url)) {
              el.errors.push(this.alerts.url);
              isValid = false;
            }
            break;
          case 'blank':
            if (elValue == '') {
              isValid = false;
              el.errors.push("Please enter a value");
            }
            break;
          case 'length':
            if (el.tagName.toLowerCase() != 'select') {
              var ruleArgs = validators[n].match(/\[(.*?)\]/g).toString()
                .replace(/\[|\]/g, '')
                .split(/\s*,\s*/);

              isValid = this.validateRegex(el, 'length', ruleArgs);
              el.errors.push(this.msg);
            }
            break;
          case 'words':
            if (el.tagName.toLowerCase() != 'select') {
              var ruleArgs = validators[n].match(/\[(.*?)\]/g).toString()
                .replace(/\[|\]/g, '')
                .split(/\s*,\s*/);
              isValid = this.validateWords(el, ruleArgs);
              el.errors.push(this.msg);
            }
            break;
          case 'differs':
            if (el.tagName.toLowerCase() != 'select') {
              var ruleArgs = validators[n].match(/\[(.*?)\]/g).toString()
                .replace(/\[|\]/g, '')
                .split(/\s*,\s*/);
              isValid = this.validateDiffers(el, ruleArgs);
              el.errors.push(this.msg);
            }
            break;
          case 'confirm':
            if (el.tagName.toLowerCase() != 'select') {
              var ruleArgs = validators[n].match(/\[(.*?)\]/g).toString()
                .replace(/\[|\]/g, '')
                .split(/\s*,\s*/);
              isValid = this.validateConfirm(el, ruleArgs);
              el.errors.push(this.msg);
            }
            break;
          case 'select':
            if (el.tagName.toLowerCase() != 'textarea') {
              isValid = this.simpleValidate(el);
              el.errors.push(this.msg);
            }
            break;
          case 'checkbox':
            if (el.tagName.toLowerCase() != 'textarea') {
              isValid = this.simpleValidate(el);
              el.errors.push(this.msg);
            }
            break;
        }
      }

      jQuery('#tooltip').remove();
      jQuery('body').find('.has-error').removeClass('has-error');

      if( el.errors.length > 0 ){

        if(this.arrayIndex(el.errors , undefined) != -1){
            var index = this.arrayIndex(el.errors , undefined);
            el.errors.splice(index,1);
        }

        if(el.errors.length > 0 ){
          var errorMessages = "";
          var element = ( target != null ? this.getElement(target) : el);
          var parentDiv = jQuery(element).parent();
          var table = this.addTable(parentDiv , el.errors);

          jQuery(table).appendTo('body');

          var height = jQuery('#tooltip').height();
          var width = jQuery(element).width();

          jQuery('#tooltip').attr('style', 'position: absolute !important')
                       .css('opacity', 1 )
                       .css('left', jQuery(element).offset().left + width - 45 )
                       .css('top', jQuery(element).offset().top - height );

          jQuery('.close').click(function(){
            jQuery('#tooltip').remove();
            jQuery('body').find('.has-error').removeClass('has-error');
          });

          jQuery('html, body').animate({
              scrollTop: jQuery(element).offset().top - height
          }, 1000);

          jQuery(window).resize(function(){
            jQuery('#tooltip').attr('style', 'position: absolute !important')
                         .css('opacity', 1 )
                         .css('left', jQuery(element).offset().left + width - 45 )
                         .css('top', jQuery(element).offset().top - height );
          });

          jQuery(parentDiv).addClass("has-error");

          return false;
        }

        return true;
      }

      return true;
    },
    simpleValidate: function(el) {

      if (el.type == 'select-one' && el.selectedIndex <= 0) {
        el.errors.push(this.alerts.select);
        return false;
      } else if (el.type == "checkbox" && el.checked == false) {
        el.errors.push(this.alerts.checkbox);
        return false;
      }
      return true;
    },
    validateGroup : function(el) {
  		el.errors = [];

  		var nlButtonGroup = this.form[jQuery(el).attr('name')];
  		el.group = nlButtonGroup;
  		var cbCheckeds = false;

  		for(var i = 0; i < nlButtonGroup.length; i++) {
  			if(nlButtonGroup[i].checked) {
  				cbCheckeds = true;
  			}
  		}

  		if(cbCheckeds == false) {
  			el.errors.push(this.alerts.radios);
  			return false;
  		} else {
  			return true;
  		}
  	},
    validateRegex: function(el, ruleMethod, ruleArgs) {

      var val1 = parseInt(ruleArgs[0]);
      var val2 = parseInt(ruleArgs[1]);

      if (val2 == -1) {
        if (el.value.length < val1) {
          el.errors.push(this.alerts.lengthmin.replace("%0", val1));
          return false;
        }
      } else if (val1 == val2) {
        if (el.value.length != val1) {
          el.errors.push(this.alerts.length_fix.replace("%0", val1));
          return false;
        }
      } else {
        if ((el.value.length < val1) || (el.value.length > val2)) {
          el.errors.push(this.alerts.length_str.replace("%0", val1).replace(
            "%1", val2));
          return false;
        }
      }

      return true;
    },
    validateDiffers: function(el, ruleArgs) {

      var differs = ruleArgs[0];

      if (document.getElementById(differs).value == el.value ||
        document.getElementsByName(differs)[0].value == el.value) {
        el.errors.push(this.alerts.differs.replace("%0", differs));
        return false;
      }
      return true;
    },
    validateWords: function(el, ruleArgs) {

      var min = ruleArgs[0];
      var max = ruleArgs[1];

      var words = el.value.replace(/[ \t\v\n\r\f\p]/m, ' ')
        .replace(/[,.;:]/g, ' ').split(' ');

      if (max == -1) {
        if (words.length < min) {
          el.errors.push(this.alerts.words_min.replace("%0",
            min).replace(
            "%1", words.length));
          return false;
        }
      } else {

        if (min > 0) {
          if (words.length < min || words.length > max) {
            el.errors.push(this.alerts.words_range.replace(
              "%0", min).replace(
              "%1", max).replace("%2", words.length));
            return false;
          }
          return true;
        } else {
          if (words.length > max) {
            el.errors.push(this.alerts.words_max.replace("%0",
              max).replace(
              "%1", words.length));
            return false;
          }
          return true;
        }
      }
    },
    validateConfirm: function(el, ruleArgs) {

      var differs = ruleArgs[0];

      if (document.getElementById(differs).value != el.value ||
        document.getElementsByName(differs)[0].value != el.value) {
        el.errors.push(this.alerts.confirm.replace("%0", differs));
        return false;
      }
      return true;
    },
    getIndex : function(array){
      var index = null ;
      jQuery.each(array,function(ind,val){
        if(val.match(/^target:.+/)){
            index=ind;
            return false;
        }
      });

      return index;
    },
    arrayIndex : function( array , needle){
      var i = -1, index = -1;

      for(i = 0; i < array.length; i++) {
          if(array[i] === needle) {
              index = i;
              break;
          }
      }

      return index;
    },
    getElement : function (value){
      return document.getElementById(value);
    },
    addListenerMulti: function (el, s, fn) {

      var evts = s.split(' ');
      for (var i=0, iLen=evts.length; i<iLen; i++) {
        el.addEventListener(evts[i], fn, false);
      }
    },
    addTable : function(el , messageArray){

        var table = '<div id="tooltip" class="tooltip fc-tbx" style="opacity:1;-moz-opacity: 1;filter: alpha(opacity=100);">';
            table += '<table cellpadding="0" cellspacing="0" border="0">';
            table += '<tbody>';
            table += '<tr>';
            table += '<td class="tl"></td>';
            table += '<td class="t"></td>';
            table += '<td class="tr"></td>';
            table += '</tr>';
            table += '<tr>';
            table += '<td class="l"></td>';
            table += '<td class="c" style="white-space: nowrap;">';
            table += '<div class="err">';

                  for(var k = 0 ; k < messageArray.length ; k++){
                      table += "<p>" + messageArray[k] + "</p>";
                  }

            table += '</div>';
            table += '<a class="close"></a>';
            table += '</td>';
            table += '<td class="r"></td>';
            table += '</tr>';
            table += '<tr>';
            table += '<td class="bl"></td>';
            table += '<td class="b"></td>';
            table += '<td class="br"></td>';
            table += '</tr>';
            table += '</tbody>';
            table += '</table>';
            table += '</div>';

        return table;
    }
  });
});